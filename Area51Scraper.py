# Area51Scraper.py


from bs4 import BeautifulSoup
from urllib.request import urlopen
import sys
import datetime
from StackExchange3DP_6 import RetrieveWebData
from StackExchange3DP_6 import stringStripperRow
from StackExchange3DP_6 import str2num
import pickle
import os
import fnmatch

DEBUG = True
url51 = 'https://area51.stackexchange.com/proposals/82438/3d-printing'
urlLong = 'https://3dprinting.meta.stackexchange.com/questions/264/what-does-it-take-to-get-out-of-beta-stage/265#265'
urlShort = 'https://3dprinting.meta.stackexchange.com/a/265/4762'
url3DP = urlLong

strFilename = '3DPCSV'
strPickleFileExtension = '.pkl'
strTextFileExtension = '.txt'
strPath = 'DataStore'
strBackupFilename = strFilename + strPickleFileExtension
strBackupPathFilename = strPath + '/' + strBackupFilename
strBackupTextFilename = strFilename + strTextFileExtension
strBackupPathTextFilename = strPath + '/' + strBackupTextFilename

strSeparator = '\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n'
strLabelCSV = 'CSV:'
strLabelMarkdown = 'Markdown:'
strLabelMarkdown0scar = '0scar\'s Markdown:'

"""
Check if we already have a local copy
Read existing csv if no local copy
Check we don't already have today's data
Scrape page
Add new data and date
Save csv
"""


def thisFunctionName():
    """Returns a string with the name of the function it's called from"""
    return sys._getframe(1).f_code.co_name


def logit(s):
    if DEBUG:
        print(s)


def stripOutUselessData(stats1, stats2):
    """Remove unwanted entries in list"""
    logit(f'Entering {thisFunctionName()}()')

    # stats1new = stats1[0:2, 4:-1]
    # https://railsware.com/blog/python-for-machine-learning-indexing-and-slicing-for-lists-tuples-strings-and-other-sequential-types/
    del (stats1[3:4])

    stats2new = stats2[1:4]

    return stats1, stats2new


def removeUsersText(data):
    """Remove unwanted ' users' in list"""
    logit(f'Entering {thisFunctionName()}()')

    # for item in data:
    #     item=item.replace('users', '')
    # https://codescracker.com/python/program/python-program-remove-word-from-sentence.htm
    for i in range(len(data)):
        data[i] = data[i].replace(' users', '')

    return data


def mergeStats(data1, data2):
    """Merge the two stat lists into one list of stats"""
    logit(f'Entering {thisFunctionName()}()')

    data = data1
    data[3:3] = data2[1:]

    return data


def removePercentSign(data):
    """Remove the percent sign in the 2nd element"""
    logit(f'Entering {thisFunctionName()}()')

    # data[1].strip('%')
    data[1] = data[1].replace('%', '')
    return data


def removeCommaSign(data):
    """Remove the percent sign in the 2nd element"""
    logit(f'Entering {thisFunctionName()}()')

    # data[1].strip('%')
    for i in range(len(data)):
        data[i] = data[i].replace(',', '')
    return data


def getData51Soup(url):
    """Using BeautifulSoup - Get the csv block as a string and return a list of lists"""
    logit(f'Entering {thisFunctionName()}()')

    page = urlopen(url)
    html = page.read().decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")

    stat_elems = soup.find_all(class_='site-health-value')
    logit(f'stat_elems:\n{stat_elems}')
    stats1 = []
    for stat_elem in stat_elems:
        print(stat_elem.text)
        stats1.append(stat_elem.text.strip())
    logit(f'stats1:\n{stats1}')

    stats2 = []
    user_elems = soup.find_all(class_='site-health-status-excellent')
    logit(f'user_elems:\n{user_elems}')
    for user_elem in user_elems:
        print(user_elem.text)
        stats2.append(user_elem.text.strip())
    logit(f'stats2:\n{stats2}')

    stats1, stats2 = stripOutUselessData(stats1, stats2)
    logit(f'stats1:\n{stats1}')
    logit(f'stats2:\n{stats2}')

    stats1 = removePercentSign(stats1)
    logit(f'stats1:\n{stats1}')

    stats1 = removeCommaSign(stats1)
    logit(f'stats1:\n{stats1}')

    stats2 = removeUsersText(stats2)
    logit(f'stats2:\n{stats2}')

    data = mergeStats(stats1, stats2)
    logit(f'data:\n{data}')

    data = stringStripperRow(data, start=0)
    logit(f'data:\n{data}')

    return data


def makeDate():
    """Make date YYYMMDD"""
    logit(f'Entering {thisFunctionName()}()')

    strNow = datetime.datetime.now(tz=datetime.timezone.utc).strftime('%Y%m%d')
    logit(f'date:\n{strNow}')
    return strNow


def makeTodaysList(date, data):
    """Make a list of seven lists - one two entry list for each data set:
    [[data1, date],[data2, date],[data3, date], ..., [dataN, date]]"""
    logit(f'Entering {thisFunctionName()}()')

    todayList = []
    dateNum = str2num(date)
    for item in data:
        todayList.append([item, dateNum])
    logit(f'todayList:\n{todayList}')

    return todayList


def getLastFile():
    """Return latest pickle file - uses fnmatch"""
    logit(f'Entering {thisFunctionName()}()')
    # Using fnmatch: https://stackoverflow.com/a/61359508/4424636
    # Or use glob: https://stackoverflow.com/a/12093995/4424636

    files = []
    globFileName = strFilename + '_*' + strPickleFileExtension
    logit(f'Glob pathFileName:\n{globFileName}')

    for filename in os.listdir(strPath):
        if fnmatch.fnmatch(filename, globFileName):
            files.append(filename)

    if files:  # if files != []:  # https://stackoverflow.com/a/53752/4424636
        latestFile = max(files)
        logit(f'latestFile:\n{latestFile}')
        latestPathFile = strPath + '/' + latestFile
    else:
        latestPathFile = ''

    return latestPathFile


# def getLastFile():
#     """Return latest pickle file"""
#     logit(f'Entering {thisFunctionName()}()')
#
#     import fnmatch
#
#     files = []
#     theFileName = strFilename + '_*' + strPickleFileExtension
#
#     thePathFileName = strPath + '/' + theFileName
#     logit(f'pathFileName:\n{thePathFileName}')
#
#     try:
#         listing = os.listdir(strPath)
#     except IOError:
#         print('File not present')
#         listing = None
#
#     if listing is not None:
#         # for filename in os.listdir(strPath):
#         for filename in listing:
#             logit(f'here!!')
#
#             # if fnmatch.fnmatch(filename, "filegen_*.pkl"):
#             # if fnmatch.fnmatch(filename, thePathFileName):
#             if fnmatch.fnmatch(filename, theFileName):
#                 files.append(filename)
#                 logit(f'here!!')
#
#         latestFile = max(files)
#         logit(f'latestFile:\n{latestFile}')
#         latestPathFile = strPath + '/' + latestFile
#     else:
#         latestPathFile = None
#
#     return latestPathFile


def readData():
    """Read File"""
    logit(f'Entering {thisFunctionName()}()')

    lastfile = getLastFile()

    try:  # https://linuxize.com/post/python-check-if-file-exists/
        # with open(backupPathFilename, 'rb') as f:
        print(f'--- Reading data from {lastfile}')
        with open(lastfile, 'rb') as f:
            data = pickle.load(f)
    except IOError:
        print('--- File not present')
        data = None
    return data


def saveText(f, string, markdown, markdown0scar):
    """Save text file"""
    logit(f'Entering {thisFunctionName()}()')

    f.write(strSeparator)
    f.write(strLabelCSV)
    f.write('\n')
    f.write(string)
    f.write(strSeparator)
    f.write(strLabelMarkdown)
    f.write('\n')
    f.write(markdown)
    f.write(strSeparator)
    f.write(strLabelMarkdown0scar)
    f.write('\n')
    f.write(markdown0scar)
    f.write(strSeparator)


def saveData(date, data, string, markdown, markdown0scar):
    """Save file"""
    logit(f'Entering {thisFunctionName()}()')

    timestampFilename = strFilename + '_' + date + strPickleFileExtension
    timestampPathFilename = strPath + '/' + timestampFilename
    # textFilename = strFilename+'_'+date+strTextFileExtension
    # textPathFilename = strPath+'/'+textFilename
    timestampTextFilename = strFilename + '_' + date + strTextFileExtension
    timestampTextPathFilename = strPath + '/' + timestampTextFilename

    # with open('Resources/3DPCSV.pkl', 'wb') as f:
    with open(strBackupPathFilename, 'wb') as f:
        pickle.dump(data, f)
        pickle.dump(string, f)
    # with open('Resources/3DPCSV.pkl', 'wb') as f:
    with open(timestampPathFilename, 'wb') as f:
        pickle.dump(data, f)
        pickle.dump(string, f)
    with open(strBackupPathTextFilename, 'w') as f:
        saveText(f, string, markdown, markdown0scar)
    with open(timestampTextPathFilename, 'w') as f:
        saveText(f, string, markdown, markdown0scar)

    print(f'--- Data saved to {timestampPathFilename}')
    print(f'--- Strings saved to {timestampTextPathFilename}')


def addNewData(todayList, data):
    """Append the new data"""
    logit(f'Entering {thisFunctionName()}()')

    for i in range(len(data)):
        data[i].append(todayList[i][0])
        data[i].append(todayList[i][1])
        # data[-1:-1] = list[0:-1]
        # data[-1:-1] = list[:]

    logit(f'data:\n{data}')

    return data


def checkHaveTodayData(today, data):
    """Return true is we already have data for today"""
    logit(f'Entering {thisFunctionName()}()')
    haveAlready = False

    if data[0][len(data[0]) - 1] == str2num(today):
        haveAlready = True

    return haveAlready


def makeBlockString(data):
    """Return block of string CSV data to copy and paste"""
    logit(f'Entering {thisFunctionName()}()')

    string = ''
    for row in data:
        stringRow = row[0]

        # for item in row:
        #     stringRow = stringRow+','+str(item)

        for i in range(1, len(row)):
            stringRow = stringRow + ',' + str(row[i])
        logit(f'stringRow:\n{stringRow}')
        string = string + stringRow + '\n'
    logit(f'string:\n{string}')
    return string


def makeBlockHTML(data):
    """Return block of string HTML data to copy and paste"""
    logit(f'Entering {thisFunctionName()}()')

    string = ''
    stringRows = ['', '', '', '', '', '', '']
    strFractions = ['150', '10', '5']

    # Get last item
    i = 0  # index of string row
    for row in data:
        if i == 1:  # For second row, answer rate, add a %
            stringRows[i] = str(row[-2]) + '&nbsp;%'
        elif 1 < i < 5:  # If user rep, add the fraction
            stringRows[i] = str(row[-2]) + '/' + strFractions[i - 2]
        else:
            stringRows[i] = str(row[-2])
        logit(f'row[-2]\n{row[-2]}')
        i = i + 1  # increment index of string row

    logit(f'stringRows\n{stringRows}')

    # Work through list, start at penultimate value and work backwards
    i = 0  # index of string row
    for row in data:
        logit(f'row\n{row}')
        for j in range(len(row) - 4, 0, -2):
            # logit(f'i: {i}')
            # logit(f'j: {j}')
            logit(f'i: {i}, j: {j}')
            if row[j] != row[j + 2]:
                if j == 1:  # If it is the first entry, bold the value and follow with a arrow
                    strArrow = ' ->'
                    strBold = '**'
                else:
                    strArrow = ''
                    strBold = ''

                # if i == 1:  # if second row, this is answer rate, put in a %
                #     stringRows[i] = '<strike>' + strBold + str(row[j]) + '&nbsp;%' + strBold + '</strike>' + strArrow + ' ' + stringRows[i]
                # else:
                #     stringRows[i] = '<strike>' + strBold + str(row[j]) + strBold + '</strike>' + strArrow + ' ' + stringRows[i]
                if i == 1:  # if second row, this is answer rate, put in a %
                    strPercent = '&nbsp;%'
                else:
                    strPercent = ''

                if 1 < i < 5:  # If user rep, add the fraction
                    strDenominator = '/' + strFractions[i - 2]
                else:
                    strDenominator = ''

                if j == 21 and 1 < i < 5:
                    strAsterisk = '<sup>*</sup>'
                else:
                    strAsterisk = ''

                stringRows[i] = '<strike>' + strBold + str(
                    row[j]) + strDenominator + strPercent + strBold + '</strike>' + strAsterisk + strArrow + ' ' + \
                                stringRows[i]
                logit(f'row[i]\n{row[i]}')
                logit(f'stringRows[i]\n{stringRows[i]}')
        i = i + 1  # increment index of string row

    # Add/prepend the headings
    i = 0  # index of string row
    for row in data:
        if 1 < i < 5:  # If user rep, add an extra indent to nest the list of user rep
            strIndent = '  '
        else:
            strIndent = ''
        if i == 5:  # if sixth row, this is question:answer rate, put in 'ratio is '
            strRatio = 'ratio is '
        else:
            strRatio = ''
        stringRows[i] = strIndent + ' - ' + row[0] + ' ' + strRatio + stringRows[i]
        logit(f'stringRows[i]\n{stringRows[i]}')
        i = i + 1  # increment index of string row

    # Put the rows into one big block string
    i = 0  # index of string row
    for row in stringRows:
        if i == 2:  # Add the Users list heading
            string = string + ' - *Users*\n'
        string = string + row + '\n'
        i = i + 1  # increment index of string row

    logit(f'String:\n{string}')
    return string


def make0scarBlockHTML(data):
    """Return block of string HTML data to copy and paste"""
    logit(f'Entering {thisFunctionName()}()')

    string = ''
    stringRows = ['', '', '', '', '', '', '']
    strFractions = ['150', '10', '5']

    # Work through list, start at penultimate value and work backwards
    i = 0  # index of string row
    for row in data:
        logit(f'row\n{row}')
        for j in range(len(row) - 4, 0, -2):
            # logit(f'i: {i}')
            # logit(f'j: {j}')
            logit(f'i: {i}, j: {j}')
            if row[j] != row[j + 2]:
                if i == 1:  # if second row, this is answer rate, put in a %
                    stringRows[i] = '<strike>' + str(row[j]) + '&nbsp;%' + '</strike>' + ' ' + stringRows[i]
                elif 1 < i < 5:  # If user rep... # ... add the fraction
                    if j == 21:  # ... and the asterisk
                        stringRows[i] = '<strike>' + str(row[j]) + '/' + strFractions[i - 2] + '</strike>' + '<sup>*</sup>' + ' ' + stringRows[i]
                    else:        # just the fraction
                        stringRows[i] = '<strike>' + str(row[j]) + '/' + strFractions[i - 2] + '</strike>' + ' ' + stringRows[i]
                # elif j == 21 and 1 < i < 5:
                #     stringRows[i] = '<strike>' + str(row[j]) + '/' + strFractions[i - 2] + '</strike>' + '<sup>*</sup>' + ' ' + stringRows[i]
                else:
                    stringRows[i] = '<strike>' + str(row[j]) + '</strike>' + ' ' + stringRows[i]
                logit(f'row[i]\n{row[i]}')
                logit(f'stringRows[i]\n{stringRows[i]}')
        i = i + 1  # increment index of string row

    # Get last item
    i = 0  # index of string row
    for row in data:
        if i == 1:  # For second row, answer rate, add a %
            stringRows[i] = '**' + str(row[-2]) + '&nbsp;%' + '**' + ' -> ' + stringRows[i]
        elif 1 < i < 5:  # If user rep, add the fraction
            stringRows[i] = '**' + str(row[-2]) + '/' + strFractions[i - 2] + '**' + ' -> ' + stringRows[i]
        else:
            stringRows[i] = '**' + str(row[-2]) + '**' + ' -> ' + stringRows[i]
        logit(f'row[-2]\n{row[-2]}')
        i = i + 1  # increment index of string row

    logit(f'stringRows\n{stringRows}')

    # Add/prepend the headings
    i = 0  # index of string row
    for row in data:
        if 1 < i < 5:  # If user rep, add an extra indent to nest the list of user rep
            strIndent = '  '
        else:
            strIndent = ''
        if i == 5:  # if sixth row, this is question:answer rate, put in 'ratio is '
            strRatio = 'ratio is '
        else:
            strRatio = ''
        stringRows[i] = strIndent + ' - ' + row[0] + ' ' + strRatio + stringRows[i]
        logit(f'stringRows[i]\n{stringRows[i]}')
        i = i + 1  # increment index of string row

    # Put the rows into one big block string
    i = 0  # index of string row
    for row in stringRows:
        if i == 2:  # Add the Users list heading
            string = string + ' - *Users*\n'
        string = string + row + '\n'
        i = i + 1  # increment index of string row

    logit(f'string:\n{string}')
    return string


def checkAndMakeDirs(dirName):
    """Check the directory to save the data in exists and create if not. Return True if dir created"""
    logit(f'Entering {thisFunctionName()}()')

    flag_directory_created = False

    # https://thispointer.com/how-to-create-a-directory-in-python
    # if not os.path.exists(dirName):
    if not checkDirs(dirName):
        os.makedirs(dirName)
        # print("Directory ", dirName, "' Created ")
        print(f"--- Directory '{dirName}' Created")
        flag_directory_created = True
    else:
        # print("Directory '", dirName, "' already exists")
        print(f"--- Directory '{dirName}' already exists")
    return flag_directory_created


def checkDirs(dirName=strPath):
    """Check the directory to save the data in exists. Return True if exists"""
    logit(f'Entering {thisFunctionName()}()')

    # https://thispointer.com/how-to-create-a-directory-in-python
    return os.path.exists(dirName)


def main():
    """This is where it all begins"""
    logit(f'Entering {thisFunctionName()}()')

    flgEmptyDir = checkAndMakeDirs(strPath)

    localCSV = readData()
    logit(f'localCSV:\n{localCSV}')

    # If local CSV doesn't exist
    if localCSV is None:
        print(f'--- Data comes from remote web: {url3DP}')
        localCSV = RetrieveWebData(url3DP)
        flgEmptyDir = True
    else:
        print(f'--- Data comes from local storage: {strPath}/')

    logit(f'localCSV:\n{localCSV}')

    dateToday = makeDate()
    haveToday = checkHaveTodayData(dateToday, localCSV)

    # if we don't have today's data already
    if not haveToday:
        print('--- Retrieving Area51 data')
        data51 = getData51Soup(url51)
        todayList = makeTodaysList(dateToday, data51)

        localCSV = addNewData(todayList, localCSV)

        stringCSV = makeBlockString(localCSV)
        stringHTML = makeBlockHTML(localCSV)
        stringHTML0scar = make0scarBlockHTML(localCSV)

        saveData(dateToday, localCSV, stringCSV, stringHTML, stringHTML0scar)
    else:
        print('--- We already have today\'s data')

        stringCSV = makeBlockString(localCSV)
        stringHTML = makeBlockHTML(localCSV)
        stringHTML0scar = make0scarBlockHTML(localCSV)

        if flgEmptyDir:
            saveData(dateToday, localCSV, stringCSV, stringHTML, stringHTML0scar)

    print(strSeparator)
    # print(f'CSV:\n{stringCSV}')
    # print(f'Markdown:\n{stringHTML}')
    # print(f'0scar\'s Markdown:\n{stringHTML0scar}')
    print(f'{strLabelCSV}\n{stringCSV}')
    print(f'{strLabelMarkdown}\n{stringHTML}')
    print(f'{strLabelMarkdown0scar}\n{stringHTML0scar}')


if __name__ == "__main__":
    main()

# To put data in copy and pastable string format - done
# Need headings added to string - no we alreay have the headings

# rename num to str2num
# TODO Check capitalisation of function names
# Can not use stringStripperRow as the start is different


# Save file with date, and save without date - to avoid having to find the latest date file to load - done
# TODO Data is obtained in string format, so why is it converted to data list format and then back again - just append new data and data to the initial string


#  percent  ripper in stackexchange3dp.py? There isn't one, no percent sign in CSV on meta page
# Save string as text - done
# save string in pickle - done
# load newest pickle, no need for backup - done
# dash in import filename - done
# mkdir - done
# TODO Add HTML output for the HTML formatted list of data - this is done, isn't it???
# TODO Add HTML output for 0scar's HTML formatted list of data - this is done, isn't it???

# TODO put the checkDirs() into readData()? - Done, changed to checkAndMakeDirs() and added simple checkDirs()

# Done 1.1 It should be "*Answers per question* ratio is 2.0 -> 1.9", Missing "ratio is"
# Done 1.1 Missing fractions on the users

# Done 1.1 If web data is up to date, then the fies are not saved, even if the DataStore didn't exist.
# TODO Rename make0scarBlockHTML() and makeBlockHTML() to make0scarBlockMarkdown() and makeBlockMarkdown() - maybe?
# Done 1.1 If the DataStore directory already exists, but is empty, then the data is not saved.
# Done 1.1 Asterisk is now only for the users
# Done 1.2 Asterisk added for 0scar HTML.
