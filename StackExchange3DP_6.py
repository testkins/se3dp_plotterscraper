# StackExchange3DP_6.py
# Removed redundant code, leaving:
#   Individual by Index graph only
#   Stack graphs for Users only
#   Case-Switch for getData()
# Moved graph rc stuff to function and added rcdefaults()
# Adding config file
# Moved global flags settings (individual and combined graphs) to function
# Split StringStripper() to have StringStripperRow() as well - StringStripperRow() can specify start
# TODO - remove old function names from print dictionary
# Added cfgSameRange
# Added window title
# Added rotated dates
# Added cfg for location of parasitic axes
# Added flag for DataStore from Area51 scraper - overrides Web retrieve.
# If DataStore isn't present then the built-in data is used
# Added exception handling for when no network is available during web retrieval.
# Renamed num() to str2num()
# Made parasitic Y axes relocatable (for both 3 and 4 Y axes)
# Split the parameter preparation for the 3 and 4 Y axis parasitic graphs into separate function
# Renamed flagParasiticThree to cfgThreePlotDataSet, added cfgFourPlotDataSet


# StackExchange3DP_5.py
# Added beautiful soup scrape - This was added in v4
# Moved imports to top
# Added graph save to all plots
# Added \n to all logit() calls
# Added dual plot
# Split plot functions into Ax and AxHead:
#   plotAllGraphsStack
#   plotParasite4Graph
#
#   plotIndexGraph
#   plotIndexGraphStacked
#   plotParasite3GraphGeneric
# Added plot save to:
#   plotAllGraphsStack
#   plotGraph
#   plotAllGraphs


# StackExchange3DP_4.py
# Moved main into three functions
# Code tidied up
# Introduced flags list for individual index graph
# Introduced User rep data processing for stacked graphs
# Added DataFrames (vertical and horizontal) (but not implemented)
# Colour Cycling for line graphs
# Stack graph data fix for User rep
# No legend for individual graphs
# Introduced case-switch for get data
# Half implemented BeautifulSoup web scrap - works but messy
# Added parasite graphs (three and four way)
# Dictionary switcher for getData method
# Half added PDF graph output
# Half added PNG graph output
# Added time stamp for files saved
# Fixed Regex web scrape

# StackExchange3DP_3.py
# logit(), all graphs and flags added

# StackExchange3DP_2.py
# Web retrieval - String Search and Regex. Regex not working
# More graphs added

# StackExchange3DP_1.py
# Data reorganisation
# Simple graph plot

"""
This is an example of the data:

*Questions per day*,2.1,20170317,1.9,20180525,1.6,20180705,2.1,20180707,2.7,20180815,2.1,20180903,1.7,20181015,2,20181106,2.4,20190327,3.0,20190905,2.5,20191119,3.9,20210121,2.8,20210411
*Answer rate*,96,20170317,93,20180525,95,20180705,96,20180707,96,20180815,97,20180903,98,20181015,98,20181106,96,20190327,95,20190905,94,20191119,88,20210121,88,20210411
*200+ reputation*,56,20170317,103,20180525,113,20180705,139,20180707,144,20180815,151,20180903,161,20181015,164,20181106,179,20190327,194,20190905,282,20191119,351,20210121,358,20210411
*2,000+ reputation*,4,20170317,8,20180525,9,20180705,10,20180707,11,20180815,12,20180903,14,20181015,14,20181106,17,20190327,19,20190905,22,20191119,27,20210121,27,20210411
*3,000+ reputation*,3,20170317,4,20180525,6,20180705,7,20180707,7,20180815,7,20180903,7,20181015,8,20181106,9,20190327,11,20190905,12,20191119,14,20210121,14,20210411
*Answers per question*,2.0,20170317,1.9,20180525,1.9,20180705,1.9,20180707,1.9,20180815,1.9,20180903,1.9,20181015,1.9,20181106,1.9,20190327,1.9,20190905,1.9,20191119,1.9,20210121,1.9,20210411
*Visits per day*,753,20170317,4,20180525,2324,20180705,2648,20180707,2675,20180815,2774,20180903,2844,20181015,3041,20181106,3707,20190327,2934,20190905,3290,20191119,8756,20210121,7146,20210411
"""

# ################################### Imports - start ###############################

from matplotlib import pyplot as plt
import datetime
import sys
import re
from bs4 import BeautifulSoup
from urllib.request import urlopen
import pandas as pd
from cycler import cycler

# from Resources.graph_config import *
# from Resources import graph_config  # This causes weak warnings for globals

# ################################### Imports - end ###############################


# ########################### Configuration File - start ##########################

# ### Configuration file settings (do not change)

# Use config file - this flag is obviously NOT in the config file
flagUseConfigFile = True
# List of config sections - this flag is obviously NOT in the config file
listOfCfgSections = ['individual_graphs', 'combination_graphs', 'graph_modifiers', 'multiple_y_axes',
                     'file_writing', 'web_scraper', 'runtime_config', 'graphing_params']
# ### Config file
strPathResources = 'Resources'                              # Directory of Resources (config files)
strFileConfig = 'graph_config_3.cfg'                        # Config file name

# ########################### Configuration File - end ############################


# ################################### Flags - start ###############################

# ### Runtime settings

# Get data from web?
# flagWeb = False                   # Use local data
flagWeb = True                      # Use web data
# Use the DataStore from the Area51 scraper - if available
# Overrides flagWeb
flagUseDataStore = True
# Print debug statements
DEBUG = True                        # Print debug statements

# ### Graph plot selections

""" 
Graphing Permutations:

Questions per day
Answer Rate
AllUsers
Users200
Users2k
Users3k
Answers per question
Questions & Answers
Visits per day
All on one sheet
Stacked
"""

# Individual graphs
flagQuestionsPerDay = True          # Select questions per day
flagAnswerRate = False              # Select answer rate
flagUsers2c = False                 # Select user reputation greater than 200
flagUsers2k = False                 # Select user reputation greater than 2000
flagUsers3K = False                 # Select user reputation greater than 3000
flagAnswersPerQuestion = False      # Select answers per question
flagVisitsPerDay = False            # Select site visits per day

# Combination graphs
flagAllGraphs = False               # All data on one plot - messy
flagAllUsers = True                 # Select all user reputations - Recommended: use with pure and stacked flags
flagQuestionsAndAnswers = False     # Select questions per day and answers per question

# Graph modifiers
flagOneSheet = False                # Plot all datasets on one set of axes
flagStacked = True                  # Use stacked graph plot - Only makes sense for user reputation
flagPure = True                     # Use "pure" User rep - deprecated
flagDualAxes = True                 # Plot pure user stacked and 4 Y-axis parasite in one fig (2 in 1)

# Multiple Y-Axes graph
flagParasite = True                 # Display multiple (3) Y-axis graph
flagParasiteAll = True              # Display multiple (4) Y-axis graph - needs flagParasite also set to true to enable

"""
Parasitic 3 Y-axis Graphing Permutations:

1 - Questions per day
2 - Answer Rate
3 - Users200
4 - Users2k
5 - Users3k
6 - Answers per question
7 - Visits per day
"""

# flagParasiteThree = [1, 2, 5]             # Select the data sets for the three Y-axes graph
# flagParasiteThree = [1, 2, 3]
cfgThreePlotDataSet = [1, 2, 6]             # Select the data sets for the three Y-axes graph
cfgThreePlotDataSetDefault = [1, 2, 6]      # Default - not in config file
cfgFourPlotDataSet = [1, 2, 6, 7]           # Select the data sets for the four Y-axes graph
cfgFourPlotDataSetDefault = [1, 2, 6, 7]    # Default - not in config file
flagSameRange = False               # Use the same Y-axis range - best for questions per day and answers per question
cfgSameRange = (0, 5)               # The range to be used on the the same Y-axis

# File Writing flags
"""
File Writing flags
"""

flagWriteGraph = False              # Save generated graphs
flagWritePDFNotPNG = True           # True: PDF, False: PNG
cfgNumDPI = 200                     # DPI setting for PNG
flagFileTime = True                 # Use filename timestamp
flagAccurateTime = False            # Use microseconds in filename timestamp


# Web scraper
"""
flagScrapeMethod:
        1: Regex
        2: String Search
        3: Beautiful Soup
"""

flagScrapeMethod = 1                # Method used to web scrape data from the SE.3DP Meta web page

"""
flagScrapeForPreNotCode:
        False: Look for <code>
        True:  Look for <pre>
"""

flagScrapeForPreNotCode = True      # Select which HTML tags to search for: <pre> or <code>

# ################################### Flags - end ###############################


# ############################ Graphing Parameters - start ######################

# cfgGraphSize = (3,5)                                      # (width, height) in inches - small graph
# cfgGraphSize = (8, 5)                                       # (width, height) in inches - good for 3 Y axes
cfgGraphSize = (9, 5)                                       # (width, height) in inches - good for 4 Y axes
cfgGraphSize4 = (9, 5)                                       # (width, height) in inches - not used (yet)

cfgPlotThickness = 4                                        # Line thickness
cfgMarkerThickness = 6                                      # Marker size

cfgParasiticAxesLocation = ['left', 'right']                 # Location of the 3rd and 4th parasitic axes

# Colours: red, green, blue, yellow
# cfgGraphColours = ['r', 'g', 'b', 'y']                    # Colours: red, green, blue, yellow
# Line styles
# cfgGraphLines = ['-', '--', ':', '-.']                    # Line styles

# Colours: red, green, blue, yellow, cyan, magenta, black
cfgGraphColours = ['r', 'g', 'b', 'y', 'c', 'm', 'k']       # Colours: red, green, blue, yellow, cyan, magenta, black
# Line styles (repeated to give 7, to match the number of colours))
cfgGraphLines = ['-', '--', ':', '-.', '--', ':', '-.']     # Line styles (repeated)
cfgDateRotation = 5                                         # Rotation of X-axis dates (in degrees). 0 -> horizontal
flagUseRC = False                                           # Use RC settings

# ############################ Graphing Parameters - end ########################


# ################################### Files - start #############################

# ### Graph saving
strPathSavedFile = 'Saved_Graphs'                           # Directory of saved graphs
# Filenames used, per function
# Dictionary: functionName --> filename
dictFileNames = {'plotGraph': 'pyplot_graph', 'plotAllGraphs': 'pyplot_graph_all', 'plotAllGraphsAxHead': 'pyplot_graph_all', 'plotAllGraphsStack': 'pyplot_stacked', 'plotAllGraphsStackAxHead': 'pyplot_stacked', 'plotParasite3Graph': 'pyplot_multiple(3)_y-axis', 'plotParasite4Graph': 'pyplot_multiple(4)_y-axis', 'plotParasite4GraphAxHead': 'pyplot_multiple(4)_y-axis', 'plotParasite3GraphGenericAxHead': 'pyplot_multiple(3)_y-axis', 'plotParasite4GraphGenericAxHead': 'pyplot_multiple(4)_y-axis', 'plotTwoGraphs': 'pylot_dual_axes_graph'}

# ################################### Files - end ###############################


# ################################### URLs - start ###############################

urlLong = 'https://3dprinting.meta.stackexchange.com/questions/264/what-does-it-take-to-get-out-of-beta-stage/265#265'
# urlShort = 'https://3dprinting.meta.stackexchange.com/a/265/4762'  # Short URL does not work
url3DP = urlLong

# ################################### URLs - end ###############################


# ################################### Local data - start ###############################

data3DP = [
    ['*Questions per day*', 2.1, 20170317, 1.9, 20180525, 1.6, 20180705, 2.1, 20180707, 2.7, 20180815, 2.1, 20180903,
     1.7, 20181015, 2, 20181106, 2.4, 20190327, 3.0, 20190905, 2.5, 20191119, 3.9, 20210121, 2.8, 20210411],
    ['*Answer rate*', 96, 20170317, 93, 20180525, 95, 20180705, 96, 20180707, 96, 20180815, 97, 20180903, 98, 20181015,
     98, 20181106, 96, 20190327, 95, 20190905, 94, 20191119, 88, 20210121, 88, 20210411],
    ['*200+ reputation*', 56, 20170317, 103, 20180525, 113, 20180705, 139, 20180707, 144, 20180815, 151, 20180903, 161,
     20181015, 164, 20181106, 179, 20190327, 194, 20190905, 282, 20191119, 351, 20210121, 358, 20210411],
    ['*2,000+ reputation*', 4, 20170317, 8, 20180525, 9, 20180705, 10, 20180707, 11, 20180815, 12, 20180903, 14,
     20181015, 14, 20181106, 17, 20190327, 19, 20190905, 22, 20191119, 27, 20210121, 27, 20210411],
    ['*3,000+ reputation*', 3, 20170317, 4, 20180525, 6, 20180705, 7, 20180707, 7, 20180815, 7, 20180903, 7, 20181015,
     8, 20181106, 9, 20190327, 11, 20190905, 12, 20191119, 14, 20210121, 14, 20210411],
    ['*Answers per question*', 2.0, 20170317, 1.9, 20180525, 1.9, 20180705, 1.9, 20180707, 1.9, 20180815, 1.9, 20180903,
     1.9, 20181015, 1.9, 20181106, 1.9, 20190327, 1.9, 20190905, 1.9, 20191119, 1.9, 20210121, 1.9, 20210411],
    ['*Visits per day*', 753, 20170317, 4, 20180525, 2324, 20180705, 2648, 20180707, 2675, 20180815, 2774, 20180903,
     2844, 20181015, 3041, 20181106, 3707, 20190327, 2934, 20190905, 3290, 20191119, 8756, 20210121, 7146, 20210411]
]

# ################################### Local data - end ###############################

"""
# ### Procedure:
# Retrieve data from web page
# Sanity check all dates are the same in each date column
# Extract dates
# Remove dates (optional)
# Put dates in a new list
# Convert dates to Python datetime format
# Extract headings
# Strip asterisks
# Remove headings (optional)
"""


"""
Looking for:

<h3>CSV Format</h3>
<ul>
<li>Format: <code>heading,data,date,data,date,...,data,date</code></li>
<li>Date format: <code>YYYYMMDD</code></li>
</ul>
<pre><code>*Questions per day*,2.1,20170317,1.9,20180525,1.6,20180705,2.1,20180707,2.7,20180815,2.1,20180903,1.7,20181015,2,20181106,2.4,20190327,3.0,20190905,2.5,20191119,3.9,20210121,2.8,20210411
*Answer rate*,96,20170317,93,20180525,95,20180705,96,20180707,96,20180815,97,20180903,98,20181015,98,20181106,96,20190327,95,20190905,94,20191119,88,20210121,88,20210411
*200+ reputation*,56,20170317,103,20180525,113,20180705,139,20180707,144,20180815,151,20180903,161,20181015,164,20181106,179,20190327,194,20190905,282,20191119,351,20210121,358,20210411
*2,000+ reputation*,4,20170317,8,20180525,9,20180705,10,20180707,11,20180815,12,20180903,14,20181015,14,20181106,17,20190327,19,20190905,22,20191119,27,20210121,27,20210411
*3,000+ reputation*,3,20170317,4,20180525,6,20180705,7,20180707,7,20180815,7,20180903,7,20181015,8,20181106,9,20190327,11,20190905,12,20191119,14,20210121,14,20210411
*Answers per question*,2.0,20170317,1.9,20180525,1.9,20180705,1.9,20180707,1.9,20180815,1.9,20180903,1.9,20181015,1.9,20181106,1.9,20190327,1.9,20190905,1.9,20191119,1.9,20210121,1.9,20210411
*Visits per day*,753,20170317,4,20180525,2324,20180705,2648,20180707,2675,20180815,2774,20180903,2844,20181015,3041,20181106,3707,20190327,2934,20190905,3290,20191119,8756,20210121,7146,20210411

</code></pre>
    </div>
"""

# Global flags - these are set programmatically in setFlags() - DO NOT TOUCH!!!
flagsIndividual = []
flagsCombinational = []


def setFlags():
    """Set flags for the individual and combined graphs"""
    logit(f'Entering {thisFunctionName()}()')

    global flagsIndividual
    flagsIndividual = [flagQuestionsPerDay, flagAnswerRate, flagUsers2c, flagUsers2k, flagUsers3K,
                       flagAnswersPerQuestion, flagVisitsPerDay]
    global flagsCombinational
    flagsCombinational = [flagAllGraphs, flagAllUsers, flagQuestionsAndAnswers]


def globalsConfig():  # https://stackoverflow.com/a/11799592/4424636
    """Set the globals - read from .cfg file - easy, uses globsls().update()"""
    logit(f'Entering {thisFunctionName()}()')

    # import ConfigParser
    import configparser
    import os
    # import os.path
    # from os import path

    # cfg = ConfigParser.RawConfigParser()
    cfg = configparser.RawConfigParser()
    # Disable lowercase conversion - https://stackoverflow.com/a/19359720/4424636
    # self.config = configparser.ConfigParser()
    # self.config.optionxform = str
    # Or this method - https://docs.python.org/2/library/configparser.html#ConfigParser.RawConfigParser.optionxform
    cfg.optionxform = str

    # cfg.read('graph.cfg')  # Read file
    strPathFileConfig = strPathResources + '/' + strFileConfig
    if os.path.exists(strPathFileConfig) and os.path.isfile(strPathFileConfig):
        cfg.read(strPathFileConfig)  # Read file

        # print cfg.getboolean('Settings','bla') # Manual Way to access them

        # par = dict(cfg.items("test"))
        # for p in par:
        #     par[p] = par[p].split("#", 1)[0].strip()  # To get rid of inline comments
        #
        # globals().update(par)  # Make them available globally

        for section in listOfCfgSections:
            logit(f'section:\n{section}')
            par = dict(cfg.items(section))
            for p in par:
                par[p] = par[p].split("#", 1)[0].strip()  # To get rid of inline comments
            logit(f'par:\n{par}')
            globals().update(par)  # Make them available globally
        logit(f'flagUseRC:\n{flagUseRC}')
    else:
        print('--- You want to use a config file, but no config file was found. Using defaults.')


def globalsConfig2():  # https://stackoverflow.com/a/924723/4424636
    """Set the globals - read from .cfg file = longwinded, requires each var to be read"""
    logit(f'Entering {thisFunctionName()}()')

    import configparser  # https://stackoverflow.com/a/14087705/4424636
    import os

    config = configparser.ConfigParser()
    strPathFileConfig = strPathResources + '/' + strFileConfig
    if os.path.exists(strPathFileConfig) and os.path.isfile(strPathFileConfig) and flagUseConfigFile:
        config.read(strPathFileConfig)

        global flagQuestionsPerDay
        global flagAnswerRate
        global flagUsers2c
        global flagUsers2k
        global flagUsers3K
        global flagAnswersPerQuestion
        global flagVisitsPerDay

        flagQuestionsPerDay = config.get("individual_graphs", "flagQuestionsPerDay")
        flagAnswerRate = config.get("individual_graphs", "flagAnswerRate")
        flagUsers2c = config.get("individual_graphs", "flagUsers2c")
        flagUsers2k = config.get("individual_graphs", "flagUsers2K")
        flagUsers3K = config.get("individual_graphs", "flagUsers3K")
        flagAnswersPerQuestion = config.get("individual_graphs", "flagAnswersPerQuestion")
        flagVisitsPerDay = config.get("individual_graphs", "flagVisitsPerDay")

        logit(f'flagUsers2c:\n{flagUsers2c}')  # For test only
    else:
        print('--- You want to use a config file, but no config file was found. Using defaults.')


def globalsConfig3():  # https://stackoverflow.com/a/924723/4424636
    """Set the globals - read from .cfg file, specifying data type = longwinded, requires each var to be read"""
    logit(f'Entering {thisFunctionName()}()')

    import configparser  # https://stackoverflow.com/a/14087705/4424636
    import os

    config = configparser.ConfigParser()
    # Disable lowercase conversion - https://docs.python.org/2/library/configparser.html#ConfigParser.RawConfigParser.optionxform
    # config.optionxform = str

    strPathFileConfig = strPathResources + '/' + strFileConfig
    if os.path.exists(strPathFileConfig) and os.path.isfile(strPathFileConfig):
        config.read(strPathFileConfig)
        print(f'--- Reading Config file: {strPathFileConfig}')

        global flagQuestionsPerDay
        global flagAnswerRate
        global flagUsers2c
        global flagUsers2k
        global flagUsers3K
        global flagAnswersPerQuestion
        global flagVisitsPerDay

        global flagAllGraphs
        global flagAllUsers
        global flagQuestionsAndAnswers

        global flagOneSheet
        global flagStacked
        global flagPure
        global flagDualAxes

        global flagParasite
        global flagParasiteAll
        global cfgThreePlotDataSet
        global cfgFourPlotDataSet
        global flagSameRange
        global cfgSameRange

        global flagWriteGraph
        global flagWritePDFNotPNG
        global cfgNumDPI
        global flagFileTime
        global flagAccurateTime

        global flagScrapeMethod
        global flagScrapeForPreNotCode

        global flagWeb
        global flagUseDataStore
        global DEBUG

        global cfgGraphSize
        global cfgPlotThickness
        global cfgMarkerThickness
        global cfgParasiticAxesLocation
        global cfgGraphColours
        global cfgGraphLines
        global cfgDateRotation
        global flagUseRC

        flagQuestionsPerDay = config.getboolean('individual_graphs', 'flagQuestionsPerDay')
        flagAnswerRate = config.getboolean("individual_graphs", "flagAnswerRate")
        flagUsers2c = config.getboolean("individual_graphs", "flagUsers2c")
        flagUsers2k = config.getboolean("individual_graphs", "flagUsers2K")
        flagUsers3K = config.getboolean("individual_graphs", "flagUsers3K")
        flagAnswersPerQuestion = config.getboolean("individual_graphs", "flagAnswersPerQuestion")
        flagVisitsPerDay = config.getboolean("individual_graphs", "flagVisitsPerDay")

        flagAllGraphs = config.getboolean("combination_graphs", "flagAllGraphs")
        flagAllUsers = config.getboolean("combination_graphs", "flagAllUsers")
        flagQuestionsAndAnswers = config.getboolean("combination_graphs", "flagQuestionsAndAnswers")

        flagOneSheet = config.getboolean("graph_modifiers", "flagOneSheet")
        flagStacked = config.getboolean("graph_modifiers", "flagStacked")
        flagPure = config.getboolean("graph_modifiers", "flagPure")
        flagDualAxes = config.getboolean("graph_modifiers", "flagDualAxes")

        flagParasite = config.getboolean("multiple_y_axes", "flagParasite")
        flagParasiteAll = config.getboolean("multiple_y_axes", "flagParasiteAll")
        # cfgThreePlotDataSet = config.get("multiple_y_axes", "cfgThreePlotDataSet")  # This is a list of int
        # From https://stackoverflow.com/a/58736848/4424636
        # For no []
        # cfgThreePlotDataSet = [int(x) for x in config.get('multiple_y_axes', 'cfgThreePlotDataSet').split(',')]
        # For []
        cfgThreePlotDataSet = [int(x) for x in config.get('multiple_y_axes', 'cfgThreePlotDataSet').strip('[]').split(',')]
        cfgFourPlotDataSet = [int(x) for x in config.get('multiple_y_axes', 'cfgFourPlotDataSet').strip('[]').split(',')]
        # https://stackoverflow.com/questions/335695/lists-in-configparser/30223001#comment104771606_58736848 - doesn't work
        # flagThreePlotDataSet = [x for x in config.getint('multiple_y_axes', 'cfgThreePlotDataSet').split(',')]
        # From https://stackoverflow.com/a/30223001/4424636 - same as first (From https://stackoverflow.com/a/58736848/4424636)
        # nl = config.get('multiple_y_axes', 'cfgThreePlotDataSet').split(',')
        # cfgThreePlotDataSet = [int(nl) for x in nl]
        logit(f'flagThreePlotDataSet:\n{cfgThreePlotDataSet}')
        flagSameRange = config.getboolean("multiple_y_axes", "flagSameRange")
        cfgSameRange_int = [int(x) for x in config.get('multiple_y_axes', 'cfgSameRange').strip('()').split(',')]  # Make a list first
        cfgSameRange = (cfgSameRange_int[0], cfgSameRange_int[1])  # Then create the tuple

        flagWriteGraph = config.getboolean("file_writing", "flagWriteGraph")
        flagWritePDFNotPNG = config.getboolean("file_writing", "flagWritePDFNotPNG")
        cfgNumDPI = config.getint("file_writing", "cfgNumDPI")
        flagFileTime = config.getboolean("file_writing", "flagFileTime")
        flagAccurateTime = config.getboolean("file_writing", "flagAccurateTime")

        flagScrapeMethod = config.getint("web_scraper", "flagScrapeMethod")
        flagScrapeForPreNotCode = config.getboolean("web_scraper", "flagScrapeForPreNotCode")

        flagWeb = config.getboolean("runtime_config", "flagWeb")
        flagUseDataStore = config.getboolean("runtime_config", "flagUseDataStore")
        DEBUG = config.getboolean("runtime_config", "DEBUG")

        # cfgGraphSize = config.get("graphing_params", "cfgGraphSize")  # This is a tuple
        # cfgGraphSize_int = [i.strip() for i in config.get("graphing_params", "cfgGraphSize").strip('()').split(',')]  # Make a list first
        # cfgGraphSize = (int(cfgGraphSize_int[0]), int(cfgGraphSize_int[1]))  # Then create the tuple
        cfgGraphSize_int = [int(x) for x in config.get('graphing_params', 'cfgGraphSize').strip('()').split(',')]  # Make a list first
        cfgGraphSize = (cfgGraphSize_int[0], cfgGraphSize_int[1])  # Then create the tuple
        logit(f'cfgGraphSize:\n{cfgGraphSize}')

        cfgPlotThickness = config.getint("graphing_params", "cfgPlotThickness")
        cfgMarkerThickness = config.getint("graphing_params", "cfgMarkerThickness")
        cfgParasiticAxesLocation = [i.strip().strip("'") for i in config.get("graphing_params", "cfgParasiticAxesLocation").strip('[]').split(',')]  # This is a list of strings
        logit(f'cfgParasiticAxesLocation:\n{cfgParasiticAxesLocation}')
        # From https://stackoverflow.com/a/22675825/4424636 and https://stackoverflow.com/a/1311381/4424636
        # For no []
        # cfgGraphColours = [i.strip().strip("'") for i in config.get("graphing_params", "cfgGraphColours").split(',')]  # This is a list of strings
        # For []
        cfgGraphColours = [i.strip().strip("'") for i in config.get("graphing_params", "cfgGraphColours").strip('[]').split(',')]  # This is a list of strings
        logit(f'cfgGraphColours:\n{cfgGraphColours}')
        # For no []
        # cfgGraphLines = [i.strip().strip("'") for i in config.get("graphing_params", "cfgGraphLines").split(',')]  # This is a list of strings
        # For []
        cfgGraphLines = [i.strip().strip("'") for i in config.get("graphing_params", "cfgGraphLines").strip('[]').split(',')]  # This is a list of strings
        # cfgGraphLines = config.get("graphing_params", "cfgGraphLines").split(',')  # This is a list of strings
        logit(f'cfgGraphLines:\n{cfgGraphLines}')
        cfgDateRotation = config.getint("graphing_params", "cfgDateRotation")
        flagUseRC = config.getboolean("graphing_params", "flagUseRC")

        logit(f'flagUsers2c:\n{flagUsers2c}')  # For test only
    else:
        print('--- You want to use a config file, but no config file was found. Using defaults.')


def rcSettings():
    """Set the rc settings"""
    logit(f'Entering {thisFunctionName()}()')

    plt.rc('lines', linewidth=cfgPlotThickness)
    plt.rc('axes', prop_cycle=(cycler('color', cfgGraphColours) +
                               cycler('linestyle', cfgGraphLines)))
    plt.rc('lines', markersize=cfgMarkerThickness)   # ??
    plt.rc('tick', rotation=cfgDateRotation)  # https://matplotlib.org/3.1.0/gallery/misc/customize_rc.html
    plt.rc('xtick', rotation=cfgDateRotation)  # https://matplotlib.org/3.1.0/gallery/misc/customize_rc.html
    plt.rc('xtick', ha='right')

    plt.rcParams["figure.figsize"] = (20, 3)  # from https://stackoverflow.com/a/41717533/4424636


def rcDefaults():
    """Set the rc defaults"""
    logit(f'Entering {thisFunctionName()}()')

    plt.rcdefaults()  # restore the defaults


def thisFunctionName():
    """Returns a string with the name of the function it's called from"""
    return sys._getframe(1).f_code.co_name


def logit(s):
    """Prints debug messages if the flag DEBUG is set"""
    if DEBUG:
        print(s)


def convStr2List(data):
    """Convert the web-retrieved CSV block from a string to a list of lists"""
    logit(f'Entering {thisFunctionName()}()')

    # https://stackoverflow.com/a/55870896/4424636
    data_list = []
    rows = data.split('\n')
    for i in rows:
        if i:  # If not a blank line
            data_list.append(i.split(','))
    return data_list


def getDataRegex(url):
    """Using regex - Get the csv block as a string and return a list of lists"""
    logit(f'Entering {thisFunctionName()}()')

    # page = urlopen(url)
    # html = page.read().decode("utf-8")
    html = urlopen(url).read().decode("utf-8")
    # logit(f'html:\n{html}')

    pattern = "<pre><code>.*?</code></pre>"  # This should be used, but doesn't work over multiple lines (?)
    match_results = re.search(pattern, html, re.DOTALL)  # This works!!!!
    logit(f'match_results:\n{match_results}')
    logit(f'match_results.group():\n{match_results.group()}')
    logit(f'################################################################')
    data = match_results.group()
    data = data[11:-13]  # Crop to lose the HTML tags
    logit(f'data:\n{data}')

    data = convStr2List(data)

    return data


def getDataSoup(url):
    """Using BeautifulSoup - Get the csv block as a string and return a list of lists"""
    logit(f'Entering {thisFunctionName()}()')

    data = []

    page = urlopen(url)
    html = page.read().decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")

    if flagScrapeForPreNotCode:

        logit('##########################################################################')
        logit('################# Search for <pre> tag ###################################')
        logit('##########################################################################')

        pre_tags = soup.find_all("pre")
        logit(f'pre_tags:\n{pre_tags}')
        for pre_elem in pre_tags:
            codepre_elem = pre_elem.find('code')
            logit(f'codepre_elem:\n{codepre_elem}')
            logit(f'codepre_elem.text:\n{codepre_elem.text}')
            data = codepre_elem.text
            data = convStr2List(data)
            logit(f'data from <pre>\n{data}')

    else:

        logit('##########################################################################')
        logit('################# Search for <code> tag ##################################')
        logit('##########################################################################')

        code_tags = soup.find_all("code")  # There are multiple code tags
        logit(f'code_tags:\n{code_tags}')

        for code_elem in code_tags:
            # rep_elem = code_elem.find_all('code', string='reputation')
            # logit(f'rep_elem:\n{rep_elem}')
            # if rep_elem != []:
            #     logit(f'rep_elem.text:\n{rep_elem.text}')

            logit(f'code_elem:\n{code_elem}')

            code_text = code_elem.text
            logit(f'code_text:\n{code_text}')
            # if code_text != '':
            #     logit(f'code_text:\n{code_text}')
            logit(f"code_text.find('reputation'):\n{code_text.find('reputation')}")

            if code_text.find('reputation') != -1:
                logit(f'Found it!!!!!!\n{code_text}')
                data = code_text
                data = convStr2List(data)
                logit(f'data from <code>\n{data}')

    # logit('##########################################################################')
    # logit('##########################################################################')
    # logit('##########################################################################')
    #
    # pre_tag = soup.select("pre")
    # logit(f'pre_tag:\n{pre_tag}')
    # # logit(f'pre_tag.text:\n{pre_tag.text}')
    # # logit(f'pre_tag.text:\n{pre_tag.}')
    #
    # logit('##########################################################################')
    # logit('##########################################################################')
    # logit('##########################################################################')
    #
    # code_tag = soup.find_all("code")
    #
    # logit(f'code_tag:\n{code_tag}')
    # logit(f'code_tag.text:\n{code_tag.text}')
    #
    # logit('##########################################################################')
    # logit('##########################################################################')
    # logit('##########################################################################')
    #
    # code_tag = soup.find_all('code', string='reputation')
    # logit(f'code_tag2:\n{code_tag}')
    # logit(f'code_tag2.text:\n{code_tag.text}')

    return data


def convertCSV(data):
    """Useless"""
    """Creates a CSV object - not required"""
    logit(f'Entering {thisFunctionName()}()')

    # https://stackoverflow.com/a/62614979/4424636
    import csv  # TODO move to top? Or delete as not used?

    logit(f'csv:\n{csv.reader(data)}')
    # data_csv = []
    data_csv = csv.reader(data)
    for row in data_csv:
        # data_csv.append(csv.reader(row))
        logit(f'row:\n{row}')
    return data_csv


def getDataString(url):
    """Using string search - Get the csv block as a string and return a list of lists"""
    logit(f'Entering {thisFunctionName()}()')

    page = urlopen(url)
    html = page.read().decode("utf-8")
    logit(f'html:\n{html}')

    pattern_start = "<pre><code>"
    # pattern_end = "</code></pre>"  # Not needed, only '<' is required
    pattern_end = "<"
    pre_index = html.find(pattern_start)
    start_index = pre_index + len(pattern_start)
    end_index = start_index + html[start_index:].find(pattern_end)
    data = html[start_index:end_index]
    logit(f'data***:\n{data}')
    # logit(f'data.shape***: {data.shape}')

    # data=convertCSV(data)
    # logit(f'data_csv: {data}')
    data = convStr2List(data)
    return data


def getData(url):
    """Just get the data using either regex or String Search, return a list"""
    logit(f'Entering {thisFunctionName()}()')

    # return getDataRegex(url)  # Not working
    # return getDataString(url)
    return getDataSoup(url)


def getData2(url):
    """Just get the data using either regex or String Search, return a list"""
    logit(f'Entering {thisFunctionName()}()')

    switcher = {
        1: getDataRegex,
        2: getDataString,
        3: getDataSoup,
    }
    # Get the function from switcher dictionary
    func = switcher.get(flagScrapeMethod, lambda: "Invalid scraping method")
    logit(f'Using {func} method to scrap data')
    # Execute the function
    # print(func(url))
    return func(url)


def rejoinKUsers(data):
    """Manually join the split 2K and 3K user headings - that were split due to a comma in the heading"""
    logit(f'Entering {thisFunctionName()}()')

    # https://stackoverflow.com/q/1142851/4424636
    # https://note.nkmk.me/en/python-string-concat/
    for row in data:
        logit(f'row[0]:\n{row[0]}')
        if row[0] == '*2' or row[0] == '*3':
            logit('joining!!!!')
            row[0:2] = [','.join(row[0:2])]
    return data

    # for i in range(0,len(data),1):
    #     # for j in range(0,len(data[i]),1):
    #         if data[i][0] == '*2' or data[i][0] == '*3':
    #             logit(f'joining!!!! ', [''.join(data[i][0:2])])
    #             data[i][0:2] = [''.join(data[i][0:2])]
    # return data


def str2num(s):
    """Return a number (float or int)"""
    logit(f'Entering {thisFunctionName()}()')

    # https://stackoverflow.com/questions/379906/how-do-i-parse-a-string-to-a-float-or-int#comment100283780_379910
    # return int(a) if float(a) == int(float(a)) else float(a)
    # or
    # https://stackoverflow.com/a/379966/4424636
    try:
        return int(s)
    except ValueError:
        return float(s)


def stringStripperRow(row, start=1):
    """Remove the single quotes around the numbers (irrespective of whether they are ints or floats"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'Before stripper:\n{row}')
    for item in range(start, len(row), 1):
        logit(f'rowitem{item}: {row[item]}')
        row[item] = str2num(row[item])
    logit(f'row:\n{row}')
    logit(f'After stripper:\n{row}')
    return row


def stringStripper(data):
    """Remove the single quotes around the numbers (irrespective of whether they are ints or floats"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'Before stripper:\n{data}')
    for row in data:
        stringStripperRow(row)
        # for item in range(1, len(row), 1):
        #     logit(f'rowitem{item}: {row[item]}')
        #     row[item] = num(row[item])
        # logit(f'row:\n{row}')
    logit(f'After stripper:\n{data}')
    return data


def interspliceData(data):
    """Mix the data up - one row is all the data for one date"""
    logit(f'Entering {thisFunctionName()}()')

    splicedData = []
    for n in range(len(data[0])):
        splicedRow = []
        for row in data:
            splicedRow.append(row[n])
        print(f'splicedRow:\n{splicedRow}')
        splicedData.append(splicedRow)
    print(f'splicedData:\n{splicedData}')

    return splicedData


def createDataFrameVertical(dates, data, headings):
    """Create a vertical (dates down) DataFrame"""
    logit(f'Entering {thisFunctionName()}()')

    df = pd.DataFrame(data, columns=headings, index=dates)
    logit(f'df:\n{df}')
    return df


def createDataFrameHorizontal(dates, data, headings):
    """Create a horizontal (dates across) DataFrame"""
    logit(f'Entering {thisFunctionName()}()')

    df = pd.DataFrame(data, columns=dates, index=headings)
    logit(f'df:\n{df}')
    return df


def sanityCheckDates(data):
    """Sanity check: all dates are consistent"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'################# Checking dates #################')

    numberOfRows = len(data)
    totalEntries = len(data[0])

    for dateColumn in range(2, totalEntries, 2):
        topRowDate = data[0][dateColumn]
        logit(f'Checking date: {topRowDate}')
        # We don't need to do the first row as this is the value against which the other rows are checked
        for row in range(1, numberOfRows, 1):
            logit(f'Checking row: {row}')
            if data[0][dateColumn] != topRowDate:
                logit(f'Date mismatch: row {row}, date: {topRowDate}')


def extractDates(data):
    """Get the dates and return in list"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'################# Extracting dates #################')

    dates = []
    totalEntries = len(data[0])

    for dateColumn in range(2, totalEntries, 2):
        topRowDate = data[0][dateColumn]
        dates.append(topRowDate)
        logit(f'Adding date: {topRowDate}')
    return dates


def extractData(data):
    """Removes dates and headings"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'################# Extracting data #################')

    numberOfRows = len(data)
    totalEntries = len(data[0])

    data3DP_noDates = []
    for row in range(0, numberOfRows, 1):
        data3DP_noDates_row = []
        logit(f'Extracting row: {row}')
        # We don't need to do the first row as this is the value against which the other rows are checked
        for dataColumn in range(1, totalEntries, 2):
            data3DP_noDates_row.append(data[row][dataColumn])
        data3DP_noDates.append(data3DP_noDates_row)
    return data3DP_noDates


def convertDate(date):
    """Convert from integer date YYYMMDD to Python date format YYYY-MM-DD"""
    logit(f'Entering {thisFunctionName()}()')

    dateString = str(date)
    # dateString_year = dateString[0:3]
    # dateString_month = dateString[4:5]
    # dateString_date = dateString[6:7]
    #
    # # pydate=datetime.datetime(date[0:3], date[4:5], date[6:7])
    # # pydate=datetime.datetime(int(dateString[0:3]), int(dateString[4:5]), int(dateString[6:7]))
    # pydate=datetime.datetime(int(dateString_year), int(dateString_month), int(dateString_date))

    # From https://www.w3schools.com/python/python_datetime.asp
    # From https://www.journaldev.com/23365/python-string-to-datetime-strptime#python-strptime
    # From https://stackoverflow.com/a/37042999/4424636

    pydate = datetime.datetime.strptime(dateString, '%Y%m%d')
    return pydate


def convertDates(dates):
    """Converts list of YYYYMMDD integer dates to python dates"""
    logit(f'Entering {thisFunctionName()}()')

    datesConverted = []
    for date in dates:
        datesConverted.append(convertDate(date))
    return datesConverted


def extractHeadings(data):
    """Returns just the heading"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'################# Extracting headings #################')

    numberOfRows = len(data)

    data3DP_headings = []
    for row in range(0, numberOfRows, 1):
        logit(f'Extracting row: {row}')
        data3DP_headings.append(data[row][0])
    return data3DP_headings


def stripAsterisks(string):
    """Strip asterisks from one string - by cropping start and end (OR by stripping)"""
    logit(f'Entering {thisFunctionName()}()')

    # By cropping
    noAsterisks = string[1:len(string) - 1]
    # By stripping
    # noAsterisks = string.strip("*")

    return noAsterisks


def stripAsterisksFromHeadings(headings):
    """Strips asterisks from all of the headings, one by one"""
    logit(f'Entering {thisFunctionName()}()')

    headingsNoAsterisks = []
    for string in headings:
        headingsNoAsterisks.append(stripAsterisks(string))
    return headingsNoAsterisks


def getPureUsers(usersData):
    """Get the pure numbers of the users with rep of: 200-1999; 2000-2999; and 3000+"""
    logit(f'Entering {thisFunctionName()}()')
    logit(f'usersData\n{usersData}')

    usersData2C_pure = []
    usersData2k_pure = []
    usersData3k_pure = []

    for i in range(len(usersData[0])):
        users2C = usersData[0][i]
        users2k = usersData[1][i]
        users3k = usersData[2][i]
        users2C_pure = users2C - users2k
        users2k_pure = users2k-users3k
        users3k_pure = users3k
        usersData2C_pure.append(users2C_pure)
        usersData2k_pure.append(users2k_pure)
        usersData3k_pure.append(users3k_pure)
    usersData_pure = [usersData2C_pure, usersData2k_pure, usersData3k_pure]
    logit(f'usersData_pure\n{usersData_pure}')

    return usersData_pure


def savePlot(strName):
    """Save the current plotted graph"""

    # if strName != None:
    if strName is not None:
        if flagFileTime:
            if flagAccurateTime:
                # strNow = datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S%f')  # https://stackoverflow.com/a/62762927/4424636
                # Do not use utcnow, use now - https://stackoverflow.com/a/62762984/4424636
                strNow = datetime.datetime.now(tz=datetime.timezone.utc).strftime('%Y%m%d%H%M%S%f')

            else:
                # strNow = datetime.datetime.utcnow().strftime('%Y%m%d%H%M%S')  # https://stackoverflow.com/a/62762927/4424636
                # Do not use utcnow, use now - https://stackoverflow.com/a/62762984/4424636
                strNow = datetime.datetime.now(tz=datetime.timezone.utc).strftime('%Y%m%d%H%M%S')
            fileName = strName+strNow
        else:
            fileName = strName

        fileName = strPathSavedFile+'/' + fileName

        if flagWriteGraph:
            if flagWritePDFNotPNG:
                # Best for professional typesetting, e.g. LaTeX
                plt.savefig(fileName+".pdf")
            else:
                # For raster graphics use the dpi argument. E.g. '[...].png", dpi=200)'
                plt.savefig(fileName+".png", dpi=cfgNumDPI)
    else:
        # functionName has no corresponding filename
        print("WARNING: No known filename to save graph - ignoring!!!!!")
        print("WARNING: No known filename to save graph - Graph has not been saved!!!!!")

# ###########################################################################################################
# #                                                                                                         #
# #                                                                                                         #
# #                               Plots                                                                     #
# #                                                                                                         #
# #                                                                                                         #
# #                                                                                                         #
# ###########################################################################################################


""" 
    Data index:

    0 - Questions per day
    1 - Answer Rate
    2 - Users200
    3 - Users2k
    4 - Users3k
    5 - Answers per question
    6 - Visits per day
"""


def plotGraph(dates, data, heading):
    """Plots one simple line graph"""
    logit(f'Entering {thisFunctionName()}()')

    x = dates
    y = data
    title = heading

    plt.grid(True)
    plt.get_current_fig_manager().set_window_title('Line Graph')  # https://stackoverflow.com/a/66042751/4424636
    plt.xlabel('Date')
    plt.ylabel(title)
    plt.title(title)
    # plt.axis([-5, 5, 0, 25])
    # plt.plot(x, y, 'b-^', linewidth=3, markersize=6, label=title)  # yellow
    plt.plot(x, y, 'b-^', linewidth=cfgPlotThickness, markersize=cfgMarkerThickness, label=title)  # yellow

    plt.legend()  # show the labels
    plt.legend(loc='upper right')  # show the labels
    savePlot(dictFileNames.get(thisFunctionName()))
    plt.show()


def plotAllGraphsIndividually(dates, data, headings):
    """Plot all graphs, individually (multiple axes), one by one"""
    logit(f'Entering {thisFunctionName()}()')

    numberOfRows = len(data)

    for row in range(0, numberOfRows, 1):
        plotGraph(dates, data[row], headings[row])


def plotAllGraphsAxHead(dates, data, heading, title='Combined stats'):
    """ Plots a simple line graph for all rows in list - iterates through the list - on one set of axes - Head that generates the ax"""
    logit(f'Entering {thisFunctionName()}()')

    x = dates
    y = data

    plt.figure(figsize=cfgGraphSize)
    plt.get_current_fig_manager().set_window_title('Line Graph')  # https://stackoverflow.com/a/66042751/4424636
    plt.grid(True)
    plt.xlabel('Date')
    plt.ylabel(title)
    plt.title(title)
    # plt.figure(figsize=cfgGraphSize)
    # plt.axis([-5, 5, 0, 25])

    # plt.rc('lines', linewidth=cfgPlotThickness)  # Not needed (here? Maybe use in rcSettings()
    # plt.rc('axes', prop_cycle=(cycler('color', cfgGraphColours) +
    #                            cycler('linestyle', cfgGraphLines)))  # Not needed (here? Maybe use in rcSettings()

    for i in range(0, len(data), 1):
        # plt.plot(x, y[i], 'b-^', linewidth=3, markersize=6, label=heading[i])
        plt.plot(x, y[i], linewidth=cfgPlotThickness, markersize=cfgMarkerThickness, label=heading[i])

    # plt.legend()  # show the labels
    plt.legend(loc='upper right')  # show the labels
    savePlot(dictFileNames.get(thisFunctionName()))
    plt.show()


def plotAllGraphsAx(dates, data, heading, ax, title='Combined stats'):
    """ Plots a simple line graph for all rows in list - iterates through the list - on one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    x = dates
    y = data

    plt.grid(True)
    plt.xlabel('Date')
    plt.ylabel(title)
    plt.title(title)
    plt.figure(figsize=cfgGraphSize)

    # plt.axis([-5, 5, 0, 25])

    # plt.rc('lines', linewidth=cfgPlotThickness)  # Not needed (here? Maybe use in rcSettings()
    # plt.rc('axes', prop_cycle=(cycler('color', cfgGraphColours) +
    #                            cycler('linestyle', cfgGraphLines)))  # Not needed (here? Maybe use in rcSettings()

    for i in range(0, len(data), 1):
        # plt.plot(x, y[i], 'b-^', linewidth=3, markersize=6, label=heading[i])
        plt.plot(x, y[i], linewidth=cfgPlotThickness, markersize=cfgMarkerThickness, label=heading[i])

    plt.legend()  # show the labels
    plt.legend(loc='upper right')  # show the labels
    plt.show()


def plotAllGraphsStackAxHead(dates, data, headings, title='Combined Stats', yLabel='Units'):
    """Plot all graphs, stack, together - but doesn't look good, only works well for pure users - Head that generates the ax"""
    logit(f'Entering {thisFunctionName()}()')

    # fig, ax = plt.subplots(figsize=(5, 3))
    fig, ax = plt.subplots(figsize=cfgGraphSize)
    # fig.canvas.set_window_title('My title')  # https://stackoverflow.com/a/30008529/4424636
    # plt.get_current_fig_manager().canvas.set_window_title('My Figure Name')  # https://stackoverflow.com/a/66042751/4424636
    plt.get_current_fig_manager().set_window_title('Stacked Graph')  # https://stackoverflow.com/a/66042751/4424636

    plotAllGraphsStackAx(dates, data, headings, ax, title, yLabel)

    fig.tight_layout()
    savePlot(dictFileNames.get(thisFunctionName()))
    plt.show()


def plotAllGraphsStackAx(dates, data, headings, ax, title='Combined Stats', yLabel='Units'):
    """Plot all graphs, stack, together - but doesn't look good, only works well for pure users - passed the ax context """
    logit(f'Entering {thisFunctionName()}()')

    logit(f'headings: {headings}')
    # fig, ax = plt.subplots(figsize=(5, 3))
    # fig, ax = plt.subplots(figsize=graphSize)
    ax.stackplot(dates, data, labels=headings)
    ax.set_title(title)
    ax.legend(loc='upper left')
    ax.set_ylabel(yLabel)
    ax.set_xlabel("Date")
    ax.set_xlim(xmin=dates[0], xmax=dates[-1])
    rotateDates(ax)


def rotateDates(ax):
    """Call appropriate rotate function"""
    logit(f'Entering {thisFunctionName()}()')

    rotateFormatAndScaleDates(ax)
    # rotateDatesOriginal(ax)
    # rotateDatesAlternateLines(ax)


def rotateFormatAndScaleDates(ax):
    """Rotate the dates using formatter and scaled translation"""
    logit(f'Entering {thisFunctionName()}()')

    # Using formatter (see https://stackoverflow.com/a/52954944/4424636)
    import matplotlib.dates as mdates
    myFmt = mdates.DateFormatter('%m-%Y')
    ax.xaxis.set_major_formatter(myFmt)
    # ax.set_xticks(ax.get)  # https://stackoverflow.com/a/21911813/4424636
    # ax.set_xticklabels(dates, rotation=cfgDateRotation)
    # ax.set_xticklabels(dates, rotation=cfgDateRotation, ha='right')
    plt.setp(ax.get_xticklabels(), rotation=cfgDateRotation, ha="right")
    # plt.show()

    # Scaled translation
    # From https://stackoverflow.com/questions/14852821/aligning-rotated-xticklabels-with-their-respective-xticks
    # From this answer https://stackoverflow.com/a/67459618/4424636
    # This actually works well
    # create offset transform (x=5pt)
    from matplotlib.transforms import ScaledTranslation
    fig = plt.gcf()  # https://stackoverflow.com/a/38433583/4424636
    dx, dy = 5, 0
    offset = ScaledTranslation(dx / fig.dpi, dy / fig.dpi, scale_trans=fig.dpi_scale_trans)

    # apply offset transform to all xticklabels
    for label in ax.xaxis.get_majorticklabels():
        label.set_transform(label.get_transform() + offset)


def rotateDatesAlternateLines(ax):
    """Print the dates on alternate lines"""
    logit(f'Entering {thisFunctionName()}()')

    # From https://stackoverflow.com/a/47500319/4424636
    # fig, ax = plt.subplots()
    # ax.plot(x, y, 'o-')
    # ax.set_xticks(x)
    xlabels = ax.get_xticklabels()
    labels = ax.set_xticklabels(xlabels)
    for i, label in enumerate(labels):
        label.set_y(label.get_position()[1] - (i % 2) * 0.075)


def rotateDatesOriginal(ax):
    """Rotate the dates using one of the methods"""
    logit(f'Entering {thisFunctionName()}()')

    # # Rotation methods
    # Mostly from https://stackoverflow.com/q/10998621/4424636
    # option 1
    # plt.xticks(rotation=cfgDateRotation, ha='right')  # https://stackoverflow.com/a/56139690/4424636 - but donest work on ax
    # plt.xticks(rotation=cfgDateRotation)  # https://stackoverflow.com/a/60903817/4424636

    # option 3a
    # ax.set_xticklabels(dates, rotation=cfgDateRotation, ha='right')
    # ax.set_xticklabels(dates, rotation=cfgDateRotation)

    # Must use option 3a with a locator, i.e. set_xticks with set_xticklabels
    # See https://stackoverflow.com/questions/52945563/axes-set-xticklabels-breaks-datetime-format#comment92817358_52954016
    # ax.set_xticks(ax.get)  # https://stackoverflow.com/a/21911813/4424636
    # This causes ticks at the dates provided, not evenly spaced:
    # ax.set_xticks(dates)  # https://stackoverflow.com/a/21911813/4424636
    # ax.set_xticklabels(dates, rotation=cfgDateRotation)
    # This causes FixedFormatter warning:
    # ax.set_xticklabels(dates, rotation=cfgDateRotation, ha='right')

    # option 3b
    # plt.draw()
    # ax.set_xticklabels(ax.get_xticklabels(), rotation=cfgDateRotation, ha='right')

    # option 4
    # for label in ax.get_xticklabels():
    #     label.set_rotation(45)
    #     label.set_ha('right')

    # option 5
    # plt.setp(ax.get_xticklabels(), rotation=cfgDateRotation, ha='right')

    # option 6
    # This works (without horiz align):
    ax.tick_params(axis='x', labelrotation=cfgDateRotation) # https://stackoverflow.com/a/56139690/4424636
    # This fails  (see https://github.com/matplotlib/matplotlib/issues/13774):
    # ax.tick_params(axis='x', labelrotation=cfgDateRotation, ha='right') # https://stackoverflow.com/a/56139690/4424636
    # ax.tick_params(axis='x', labelrotation=cfgDateRotation, horizontalalignment='right') # https://stackoverflow.com/a/56139690/4424636


# ########################### Parasite graphs - start ###############################
# #                                                                                 #
# #                           Parasite   graphs                                     #
# #                                                                                 #
# ########################### Parasite graphs - start ###############################

def plotParasite3GraphGenericAxHead(dates, data, headings, title='Combined Stats'):
    """Print 3 Y-axis parasite graph - passed a three row data list only - Head that generates the ax"""
    logit(f'Entering {thisFunctionName()}()')

    # From https://stackoverflow.com/a/45925049/4424636

    # Create figure and subplot manually
    # fig = plt.figure()
    # host = fig.add_subplot(111)

    # More versatile wrapper
    # fig, host = plt.subplots(figsize=(8, 5))  # (width, height) in inches
    fig, host = plt.subplots(figsize=cfgGraphSize)  # (width, height) in inches
    plt.get_current_fig_manager().set_window_title('3 Y-axis Graph')  # https://stackoverflow.com/a/66042751/4424636

    # plotParasite3GraphGenericAx(dates, data, headings, host, title)
    plotRelocatableParasite3GraphGenericAx(dates, data, headings, host, title)

    # Adjust spacings w.r.t. figsize
    fig.tight_layout()
    # Alternatively: bbox_inches='tight' within the plt.savefig function
    #                (overwrites figsize)

    # strName = dictFileNames.get(thisFunctionName())
    # savePlot(strName)
    savePlot(dictFileNames.get(thisFunctionName()))

    plt.show()


def plotParasite3GraphGenericAx(dates, data, headings, ax, title='Combined Stats'):
    """Print 3 Y-axis parasite graph - passed a three row data list only - passed the ax context"""
    logit(f'Entering {thisFunctionName()}()')

    host = ax
    par1 = host.twinx()
    par2 = host.twinx()

    # Not needed - we need autoscale (maybe) Maybe need to limit AR to 100 (%)
    # host.set_xlim(0, 2)
    # host.set_ylim(0, 2)
    par1.set_ylim(0, 100)
    # par2.set_ylim(1, 65)

    host.set_xlabel("Date")
    host.set_ylabel(headings[0])
    par1.set_ylabel(headings[1])
    par2.set_ylabel(headings[2])

    # color1 = plt.cm.viridis(0)
    # color2 = plt.cm.viridis(0.5)
    # color3 = plt.cm.viridis(.9)
    color1 = cfgGraphColours[0]
    color2 = cfgGraphColours[1]
    color3 = cfgGraphColours[2]

    p1, = host.plot(dates, data[0], color=color1, label=headings[0])
    p2, = par1.plot(dates, data[1], color=color2, label=headings[1])
    p3, = par2.plot(dates, data[2], color=color3, label=headings[2])

    lns = [p1, p2, p3]
    host.set_title(title)
    host.legend(handles=lns, loc='best')

    # right, left, top, bottom
    par2.spines['right'].set_position(('outward', 60))

    # no x-ticks
    # par2.xaxis.set_ticks([])

    host.yaxis.label.set_color(p1.get_color())
    par1.yaxis.label.set_color(p2.get_color())
    par2.yaxis.label.set_color(p3.get_color())

    rotateDates(ax)


def plotRelocatableParasite3GraphGenericAx(dates, data, headings, ax, title='Combined Stats'):
    """Print 3 Y-axis parasite graph - passed a three row data list only - passed the ax context - movable Y axis (left or right)"""
    logit(f'Entering {thisFunctionName()}()')

    host = ax
    par1 = host.twinx()
    par2 = host.twinx()

    # Not needed - we need autoscale (maybe) Maybe need to limit AR to 100 (%)
    # host.set_xlim(0, 2)
    # host.set_ylim(0, 2)
    par1.set_ylim(0, 100)
    # par2.set_ylim(1, 65)

    host.set_xlabel("Date")
    host.set_ylabel(headings[0])
    par1.set_ylabel(headings[1])
    par2.set_ylabel(headings[2])

    # color1 = plt.cm.viridis(0)
    # color2 = plt.cm.viridis(0.5)
    # color3 = plt.cm.viridis(.9)
    color1 = cfgGraphColours[0]
    color2 = cfgGraphColours[1]
    color3 = cfgGraphColours[2]

    p1, = host.plot(dates, data[0], color=color1, label=headings[0])
    p2, = par1.plot(dates, data[1], color=color2, label=headings[1])
    p3, = par2.plot(dates, data[2], color=color3, label=headings[2])

    lns = [p1, p2, p3]
    host.set_title(title)
    host.legend(handles=lns, loc='best')

    # right, left, top, bottom
    # par2.spines['right'].set_position(('outward', 60))
    locationLeft = 60
    locationRight = 60
    if cfgParasiticAxesLocation[0] == 'right':
        par2.spines['right'].set_position(('outward', locationRight))
    # else:
    elif cfgParasiticAxesLocation[0] == 'left':
        par2.spines['left'].set_position(('outward', locationLeft))
        par2.spines['left'].set_visible(True)
        par2.yaxis.set_label_position('left')
        par2.yaxis.set_ticks_position('left')
    else:
        print(f'Warning!!!!!: Incorrect parameter specified.')
        print(f'Warning!!!!!: Check parameters of cfgParasiticAxesLocation[0]: {cfgParasiticAxesLocation[0]}')
        print(f'Warning!!!!!: Value should be \'left\' or \'right\'')
        print(f'Warning!!!!!: Defaulting to \'right\'')
        par2.spines['right'].set_position(('outward', locationRight))
        # print(f'Warning!!!!!: Defaulting to \'left\'')
        # par2.spines['left'].set_position(('outward', locationLeft))
        # par2.spines['left'].set_visible(True)
        # par2.yaxis.set_label_position('left')
        # par2.yaxis.set_ticks_position('left')

    # no x-ticks
    # par2.xaxis.set_ticks([])

    host.yaxis.label.set_color(p1.get_color())
    par1.yaxis.label.set_color(p2.get_color())
    par2.yaxis.label.set_color(p3.get_color())

    rotateDates(ax)


# def plotParasite4GraphAxHead(dates, data, headings, title='Combined Stats'):
#     """Print 4 Y-axis parasite graph - Head that generates the ax"""
#     logit(f'Entering {thisFunctionName()}()')
#
#     # 22-ParasiteScales-PureMatPlotLib.py
#
#     # From https://stackoverflow.com/a/45925049/4424636
#
#     # Create figure and subplot manually
#     # fig = plt.figure()
#     # host = fig.add_subplot(111)
#
#     # More versatile wrapper
#     # fig, host = plt.subplots(figsize=(8, 5))  # (width, height) in inches
#     fig, host = plt.subplots(figsize=cfgGraphSize)  # (width, height) in inches
#     # (see https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.subplots.html)
#     plt.get_current_fig_manager().set_window_title('4 Y-axis Graph')  # https://stackoverflow.com/a/66042751/4424636
#
#     plotParasite4GraphAx(dates, data, headings, host, title)
#
#     # Adjust spacings w.r.t. figsize
#     fig.tight_layout()
#     # Alternatively: bbox_inches='tight' within the plt.savefig function
#     #                (overwrites figsize)
#
#     savePlot(dictFileNames.get(thisFunctionName()))
#
#     plt.show()
#
#
# def plotParasite4GraphAx(dates, data, headings, ax, title='Combined Stats'):
#     """Print 4 Y-axis parasite graph - passing the ax context"""
#     logit(f'Entering {thisFunctionName()}()')
#
#     # 0 - Questions per day
#     # 1 - Answer rate
#     # 5 - Answers per question
#     # 6 - Visits per day
#
#     host = ax
#     par1 = host.twinx()
#     par2 = host.twinx()
#     par3 = host.twinx()
#
#     # Not needed - we need autoscale (maybe) Maybe need to limit AR to 100 (%)
#     # host.set_xlim(0, 2)
#     # Same range for QPD (0) and APQ (5)
#     if flagSameRange:
#         # host.set_ylim(0, 5)  # Same range - questions per day
#         # par2.set_ylim(0, 5)  # Same range - answers per question
#         host.set_ylim(cfgSameRange)  # Same range - questions per day
#         par2.set_ylim(cfgSameRange)  # Same range - answers per question
#     par1.set_ylim(0, 100)
#
#     host.set_xlabel("Date")
#     host.set_ylabel(headings[0])
#     par1.set_ylabel(headings[1])
#     par2.set_ylabel(headings[5])
#     par3.set_ylabel(headings[6])
#
#     # color1 = plt.cm.viridis(0)
#     # color2 = plt.cm.viridis(0.5)
#     # color3 = plt.cm.viridis(.9)
#     # color4 = plt.cm.viridis(.25)
#     color1 = cfgGraphColours[0]
#     color2 = cfgGraphColours[1]
#     color3 = cfgGraphColours[2]
#     color4 = cfgGraphColours[3]
#
#     p1, = host.plot(dates, data[0], color=color1, label=headings[0])
#     p2, = par1.plot(dates, data[1], color=color2, label=headings[1])
#     p3, = par2.plot(dates, data[5], color=color3, label=headings[5])
#     p4, = par3.plot(dates, data[6], color=color4, label=headings[6])
#
#     lns = [p1, p2, p3, p4]
#     # host.legend(handles=lns, loc='best')
#     host.set_title(title)
#     host.legend(handles=lns, loc=(0.65, 0.1))
#
#     # right, left, top, bottom
#     par3.spines['right'].set_position(('outward', 60))
#
#     # no x-ticks
#     # par2.xaxis.set_ticks([])
#
#     # Sometimes handy, same for xaxis
#     # par2.yaxis.set_ticks_position('right')
#
#     # Move "Velocity"-axis to the left
#     par2.spines['left'].set_position(('outward', 60))
#     par2.spines['left'].set_visible(True)
#     par2.yaxis.set_label_position('left')
#     par2.yaxis.set_ticks_position('left')
#
#     host.yaxis.label.set_color(p1.get_color())
#     par1.yaxis.label.set_color(p2.get_color())
#     par2.yaxis.label.set_color(p3.get_color())
#     par3.yaxis.label.set_color(p4.get_color())
#
#     rotateDates(ax)


def plotParasite4GraphGenericAxHead(dates, data, headings, title='Combined Stats'):
    """Print 4 Y-axis parasite graph - Head that generates the ax"""
    logit(f'Entering {thisFunctionName()}()')

    # 22-ParasiteScales-PureMatPlotLib.py

    # From https://stackoverflow.com/a/45925049/4424636

    # Create figure and subplot manually
    # fig = plt.figure()
    # host = fig.add_subplot(111)

    # More versatile wrapper
    # fig, host = plt.subplots(figsize=(8, 5))  # (width, height) in inches
    fig, host = plt.subplots(figsize=cfgGraphSize)  # (width, height) in inches
    # (see https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.subplots.html)
    plt.get_current_fig_manager().set_window_title('4 Y-axis Graph')  # https://stackoverflow.com/a/66042751/4424636

    # plotParasite4GraphGenericAx(dates, data, headings, host, title)
    plotRelocatableParasite4GraphGenericAx(dates, data, headings, host, title)

    # Adjust spacings w.r.t. figsize
    fig.tight_layout()
    # Alternatively: bbox_inches='tight' within the plt.savefig function
    #                (overwrites figsize)

    savePlot(dictFileNames.get(thisFunctionName()))

    plt.show()


def plotParasite4GraphGenericAx(dates, data, headings, ax, title='Combined Stats'):
    """Print 4 Y-axis parasite graph - passing the ax context"""
    logit(f'Entering {thisFunctionName()}()')

    # 0 - Questions per day
    # 1 - Answer rate
    # 5 - Answers per question
    # 6 - Visits per day

    host = ax
    par1 = host.twinx()
    par2 = host.twinx()
    par3 = host.twinx()

    # Not needed - we need autoscale (maybe) Maybe need to limit AR to 100 (%)
    # host.set_xlim(0, 2)
    # Same range for QPD (0) and APQ (5)
    if flagSameRange:
        # host.set_ylim(0, 5)  # Same range - questions per day
        # par2.set_ylim(0, 5)  # Same range - answers per question
        host.set_ylim(cfgSameRange)  # Same range - questions per day
        par2.set_ylim(cfgSameRange)  # Same range - answers per question
    par1.set_ylim(0, 100)

    host.set_xlabel("Date")
    host.set_ylabel(headings[0])
    par1.set_ylabel(headings[1])
    par2.set_ylabel(headings[2])
    par3.set_ylabel(headings[3])

    # color1 = plt.cm.viridis(0)
    # color2 = plt.cm.viridis(0.5)
    # color3 = plt.cm.viridis(.9)
    # color4 = plt.cm.viridis(.25)
    color1 = cfgGraphColours[0]
    color2 = cfgGraphColours[1]
    color3 = cfgGraphColours[2]
    color4 = cfgGraphColours[3]

    p1, = host.plot(dates, data[0], color=color1, label=headings[0])
    p2, = par1.plot(dates, data[1], color=color2, label=headings[1])
    p3, = par2.plot(dates, data[2], color=color3, label=headings[2])
    p4, = par3.plot(dates, data[3], color=color4, label=headings[3])

    lns = [p1, p2, p3, p4]
    # host.legend(handles=lns, loc='best')
    host.set_title(title)
    host.legend(handles=lns, loc=(0.65, 0.1))

    # right, left, top, bottom
    par3.spines['right'].set_position(('outward', 60))

    # no x-ticks
    # par2.xaxis.set_ticks([])

    # Sometimes handy, same for xaxis
    # par2.yaxis.set_ticks_position('right')

    # Move "Velocity"-axis to the left
    par2.spines['left'].set_position(('outward', 60))
    par2.spines['left'].set_visible(True)
    par2.yaxis.set_label_position('left')
    par2.yaxis.set_ticks_position('left')

    host.yaxis.label.set_color(p1.get_color())
    par1.yaxis.label.set_color(p2.get_color())
    par2.yaxis.label.set_color(p3.get_color())
    par3.yaxis.label.set_color(p4.get_color())

    rotateDates(ax)


def plotRelocatableParasite4GraphGenericAx(dates, data, headings, ax, title='Combined Stats'):
    """Print 4 Y-axis parasite graph - passing the ax context - movable Y axis (left or right)"""
    logit(f'Entering {thisFunctionName()}()')

    # 0 - Questions per day
    # 1 - Answer rate
    # 5 - Answers per question
    # 6 - Visits per day

    host = ax
    par1 = host.twinx()
    par2 = host.twinx()
    par3 = host.twinx()

    # Not needed - we need autoscale (maybe) Maybe need to limit AR to 100 (%)
    # host.set_xlim(0, 2)
    # Same range for QPD (0) and APQ (5)
    if flagSameRange:
        # host.set_ylim(0, 5)  # Same range - questions per day
        # par2.set_ylim(0, 5)  # Same range - answers per question
        host.set_ylim(cfgSameRange)  # Same range - questions per day
        par2.set_ylim(cfgSameRange)  # Same range - answers per question
    par1.set_ylim(0, 100)

    host.set_xlabel("Date")
    host.set_ylabel(headings[0])
    par1.set_ylabel(headings[1])
    par2.set_ylabel(headings[2])
    par3.set_ylabel(headings[3])

    # color1 = plt.cm.viridis(0)
    # color2 = plt.cm.viridis(0.5)
    # color3 = plt.cm.viridis(.9)
    # color4 = plt.cm.viridis(.25)
    color1 = cfgGraphColours[0]
    color2 = cfgGraphColours[1]
    color3 = cfgGraphColours[2]
    color4 = cfgGraphColours[3]

    p1, = host.plot(dates, data[0], color=color1, label=headings[0])
    p2, = par1.plot(dates, data[1], color=color2, label=headings[1])
    p3, = par2.plot(dates, data[2], color=color3, label=headings[2])
    p4, = par3.plot(dates, data[3], color=color4, label=headings[3])

    lns = [p1, p2, p3, p4]
    # host.legend(handles=lns, loc='best')
    host.set_title(title)
    host.legend(handles=lns, loc=(0.65, 0.1))

    # right, left, top, bottom

    # no x-ticks
    # par2.xaxis.set_ticks([])

    # Sometimes handy, same for xaxis
    # par2.yaxis.set_ticks_position('right')

    locationRight = 60
    locationLeft = 60
    locationShift = 60

    # Move "Velocity"-axis to the left
    if cfgParasiticAxesLocation[0] == 'right':
        par2.spines['right'].set_position(('outward', locationRight))
        locationRight += locationShift  # The next 'right' is shifted across
    # else:
    elif cfgParasiticAxesLocation[0] == 'left':
        par2.spines['left'].set_position(('outward', locationLeft))
        par2.spines['left'].set_visible(True)
        par2.yaxis.set_label_position('left')
        par2.yaxis.set_ticks_position('left')
        locationLeft += locationShift  # The next 'left' is shifted across
    else:
        print(f'Warning!!!!!: Incorrect parameter specified.')
        print(f'Warning!!!!!: Check parameters of cfgParasiticAxesLocation[0]: {cfgParasiticAxesLocation[0]}')
        print(f'Warning!!!!!: Value should be \'left\' or \'right\'')
        print(f'Warning!!!!!: Defaulting to \'left\'')
        par2.spines['left'].set_position(('outward', locationLeft))
        par2.spines['left'].set_visible(True)
        par2.yaxis.set_label_position('left')
        par2.yaxis.set_ticks_position('left')
        locationLeft += locationShift  # The next 'left' is shifted across

    # Move "Velocity"-axis to the left
    if cfgParasiticAxesLocation[1] == 'right':
        par3.spines['right'].set_position(('outward', locationRight))
    # else:
    elif cfgParasiticAxesLocation[1] == 'left':
        par3.spines['left'].set_position(('outward', locationLeft))
        par3.spines['left'].set_visible(True)
        par3.yaxis.set_label_position('left')
        par3.yaxis.set_ticks_position('left')
    else:
        print(f'Warning!!!!!: Incorrect parameter specified.')
        print(f'Warning!!!!!: Check parameters of cfgParasiticAxesLocation[1]: {cfgParasiticAxesLocation[1]}')
        print(f'Warning!!!!!: Value should be \'left\' or \'right\'')
        print(f'Warning!!!!!: Defaulting to \'right\'')
        par3.spines['right'].set_position(('outward', locationRight))

    host.yaxis.label.set_color(p1.get_color())
    par1.yaxis.label.set_color(p2.get_color())
    par2.yaxis.label.set_color(p3.get_color())
    par3.yaxis.label.set_color(p4.get_color())

    rotateDates(ax)


def plot3YAxisGraph(dates, data, headings):
    """Prepare the data list for the 3 Y-axis parasitic graph"""
    logit(f'Entering {thisFunctionName()}()')

    # # global flagParasiteThree
    #
    # global cfgThreePlotDataSet
    #
    # dataMini = []
    # headingsMini = []
    # if len(cfgThreePlotDataSet) != 3:  # Sanity check for only three items
    #     print(f'Warning!!!!!: Incorrect amount of datasets supplied for three Y-axis graph.')
    #     print(f'Warning!!!!!: {len(cfgThreePlotDataSet)} datasets provided, instead of 3')
    #     print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet: {cfgThreePlotDataSet}')
    #     print(f'Warning!!!!!: Defaulting to printing default dataset: {cfgThreePlotDataSetDefault}')
    #     cfgThreePlotDataSet = cfgThreePlotDataSetDefault
    #
    # for i in range(3):
    #     index = cfgThreePlotDataSet[i] - 1  # Adjust for zero index being the first
    #     if index < 0:  # Sanity check for range
    #         print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
    #         print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet[{i}]: {cfgThreePlotDataSet[i]}')
    #         print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgThreePlotDataSetDefault[i]}')
    #         # index = 0
    #         index = cfgThreePlotDataSetDefault[i]
    #     if index > 6:  # Sanity check for range
    #         print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
    #         print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet[{i}]: {cfgThreePlotDataSet[i]}')
    #         print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgThreePlotDataSetDefault[i]}')
    #         # index = 6
    #         index = cfgThreePlotDataSetDefault[i]
    #     dataMini.append(data[index])
    #     headingsMini.append(headings[index])
    #
    # plotParasite3GraphGenericAxHead(dates, dataMini, headingsMini)
    dates, dataMini, headingsMini = prepare3YAxisGraphParams(dates, data, headings)
    plotParasite3GraphGenericAxHead(dates, dataMini, headingsMini)


def prepare3YAxisGraphParams(dates, data, headings):
    """Prepare the data list for the 3 Y-axis parasitic graph"""
    logit(f'Entering {thisFunctionName()}()')

    # global flagParasiteThree

    global cfgThreePlotDataSet

    dataMini = []
    headingsMini = []
    if len(cfgThreePlotDataSet) != 3:  # Sanity check for only three items
        print(f'Warning!!!!!: Incorrect amount of datasets supplied for three Y-axis graph.')
        print(f'Warning!!!!!: {len(cfgThreePlotDataSet)} datasets provided, instead of 3')
        print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet: {cfgThreePlotDataSet}')
        print(f'Warning!!!!!: Defaulting to printing default dataset: {cfgThreePlotDataSetDefault}')
        cfgThreePlotDataSet = cfgThreePlotDataSetDefault

    for i in range(3):
        index = cfgThreePlotDataSet[i] - 1  # Adjust for zero index being the first
        if index < 0:  # Sanity check for range
            print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
            print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet[{i}]: {cfgThreePlotDataSet[i]}')
            print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgThreePlotDataSetDefault[i]}')
            # index = 0
            index = cfgThreePlotDataSetDefault[i]
        if index > 6:  # Sanity check for range
            print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
            print(f'Warning!!!!!: Check parameters of cfgThreePlotDataSet[{i}]: {cfgThreePlotDataSet[i]}')
            print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgThreePlotDataSetDefault[i]}')
            # index = 6
            index = cfgThreePlotDataSetDefault[i]
        dataMini.append(data[index])
        headingsMini.append(headings[index])

    return(dates, dataMini, headingsMini)


def plot4YAxisGraph(dates, data, headings):
    """Prepare the data list for the 4 Y-axis parasitic graph"""
    logit(f'Entering {thisFunctionName()}()')

    # global cfgFourPlotDataSet
    #
    # dataMini = []
    # headingsMini = []
    # if len(cfgFourPlotDataSet) != 4:  # Sanity check for only three items
    #     print(f'Warning!!!!!: Incorrect amount of datasets supplied for four Y-axis graph.')
    #     print(f'Warning!!!!!: {len(cfgFourPlotDataSet)} datasets provided, instead of 4')
    #     print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet: {cfgFourPlotDataSet}')
    #     print(f'Warning!!!!!: Defaulting to printing default dataset: {cfgFourPlotDataSetDefault}')
    #     cfgFourPlotDataSet = cfgFourPlotDataSetDefault
    #
    # for i in range(4):
    #     index = cfgFourPlotDataSet[i] - 1  # Adjust for zero index being the first
    #     if index < 0:  # Sanity check for range
    #         print(f'Warning!!!!!: Index of dataset supplied for four Y-axis graph out of range.')
    #         print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet[{i}]: {cfgFourPlotDataSet[i]}')
    #         print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgFourPlotDataSetDefault[i]}')
    #         index = 0
    #         index = cfgFourPlotDataSetDefault[i]
    #     if index > 6:  # Sanity check for range
    #         print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
    #         print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet[{i}]: {cfgFourPlotDataSet[i]}')
    #         print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgFourPlotDataSetDefault[i]}')
    #         # index = 6
    #         index = cfgFourPlotDataSetDefault[i]
    #     dataMini.append(data[index])
    #     headingsMini.append(headings[index])
    #
    # plotParasite4GraphGenericAxHead(dates, dataMini, headingsMini)
    # plotParasite4GraphGenericAxHead(prepare4YAxisGraphParams(dates, data, headings))
    dates, dataMini, headingsMini = prepare4YAxisGraphParams(dates, data, headings)
    plotParasite4GraphGenericAxHead(dates, dataMini, headingsMini)


def prepare4YAxisGraphParams(dates, data, headings):
    """Prepare the data list for the 4 Y-axis parasitic graph"""
    logit(f'Entering {thisFunctionName()}()')

    global cfgFourPlotDataSet

    dataMini = []
    headingsMini = []
    if len(cfgFourPlotDataSet) != 4:  # Sanity check for only three items
        print(f'Warning!!!!!: Incorrect amount of datasets supplied for four Y-axis graph.')
        print(f'Warning!!!!!: {len(cfgFourPlotDataSet)} datasets provided, instead of 4')
        print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet: {cfgFourPlotDataSet}')
        print(f'Warning!!!!!: Defaulting to printing default dataset: {cfgFourPlotDataSetDefault}')
        cfgFourPlotDataSet = cfgFourPlotDataSetDefault

    for i in range(4):
        index = cfgFourPlotDataSet[i] - 1  # Adjust for zero index being the first
        if index < 0:  # Sanity check for range
            print(f'Warning!!!!!: Index of dataset supplied for four Y-axis graph out of range.')
            print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet[{i}]: {cfgFourPlotDataSet[i]}')
            print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgFourPlotDataSetDefault[i]}')
            index = 0
            index = cfgFourPlotDataSetDefault[i]
        if index > 6:  # Sanity check for range
            print(f'Warning!!!!!: Index of dataset supplied for three Y-axis graph out of range.')
            print(f'Warning!!!!!: Check parameters of cfgFourPlotDataSet[{i}]: {cfgFourPlotDataSet[i]}')
            print(f'Warning!!!!!: Defaulting to printing element from default dataset: {cfgFourPlotDataSetDefault[i]}')
            # index = 6
            index = cfgFourPlotDataSetDefault[i]
        dataMini.append(data[index])
        headingsMini.append(headings[index])

    return(dates, dataMini, headingsMini)


def plotTwoGraphs(dates, data, headings):
    """Print 4 Y-axis parasite graph an stacked user rep graph in one fig (2 in 1)"""
    logit(f'Entering {thisFunctionName()}()')

    # global graphSize
    graphSizeTwiceAsWide = (cfgGraphSize[0]*2, cfgGraphSize[1])

    # Use this (or the alternate), they are about the same:
    # fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=graphSizeTwiceAsWide)

    # ########################################################################################################
    # Alternate (from https://realpython.com/python-matplotlib-guide/#understanding-pltsubplots-notation)
    gridsize = (1, 2)
    fig = plt.figure(figsize=graphSizeTwiceAsWide)
    # Either:
    # ax1 = plt.subplot2grid(gridsize, (0, 0))
    # ax2 = plt.subplot2grid(gridsize, (0, 1))
    # Or:
    # From https://stackoverflow.com/a/37767461/4424636
    ax1 = plt.subplot2grid(gridsize, (0, 0), aspect="auto", adjustable='box')
    ax2 = plt.subplot2grid(gridsize, (0, 1), aspect="auto", adjustable='box')
    # ########################################################################################################

    plt.get_current_fig_manager().set_window_title('Dual Graph')  # https://stackoverflow.com/a/66042751/4424636

    # plotRelocatableParasite4GraphGenericAx(dates, data, headings, ax1)
    dates, dataMini, headingsMini = prepare4YAxisGraphParams(dates, data, headings)
    plotRelocatableParasite4GraphGenericAx(dates, dataMini, headingsMini, ax1)

    plotAllUsersPureGraphsStackAx(dates, data, headings, ax2)

    fig.tight_layout()
    savePlot(dictFileNames.get(thisFunctionName()))
    plt.show()


# ############################### Line graphs - start ###############################
# #                                                                                 #
# #                                 Line graphs                                     #
# #                                                                                 #
# ############################### Line graphs - start ###############################

def plotAllUsersGraphsAxHead(dates, data, headings):
    """Combined graph - Plots 200, 2k and 3k user rep graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    headings_allUsers = headings[2:5]
    plotAllGraphsAxHead(dates, data3DP_nodates_allUsers, headings_allUsers, 'All users')


def plotAllUsersGraphsAx(dates, data, headings):  # TODO Add ax parameter - currently identical to the AxHead()
    """Combined graph - Plots 200, 2k and 3k user rep graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    headings_allUsers = headings[2:5]
    plotAllGraphsAx(dates, data3DP_nodates_allUsers, headings_allUsers, 'All users')


def plotAllUsersPureGraphsAxHead(dates, data, headings):
    """Combined graph - Plots pure 200, 2k and 3k user rep graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    data3DP_nodates_allUsersPure = getPureUsers(data3DP_nodates_allUsers)
    headings_allUsers = headings[2:5]
    plotAllGraphsAxHead(dates, data3DP_nodates_allUsersPure, headings_allUsers, 'All users')


def plotAllUsersPureGraphsAx(dates, data, headings):  # TODO Add ax parameter - currently identical to the AxHead()
    """Combined graph - Plots pure 200, 2k and 3k user rep graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    data3DP_nodates_allUsersPure = getPureUsers(data3DP_nodates_allUsers)
    headings_allUsers = headings[2:5]
    plotAllGraphsAx(dates, data3DP_nodates_allUsersPure, headings_allUsers, 'All users')


def plotQAGraphsAxHead(dates, data, headings):
    """Combined graph - Plots the questions/day and answers/question graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    # data3DP_nodates_QA=[data[0],data[5]]
    # headings_QA=[headings[0],headings[5]]
    data3DP_nodates_QA = data[0:6:5]
    headings_QA = headings[0:6:5]

    plotAllGraphsAxHead(dates, data3DP_nodates_QA, headings_QA, 'Questions/day and Answers/question')


def plotQAGraphsAx(dates, data, headings):  # TODO Add ax parameter - currently identical to the AxHead()
    """Combined graph - Plots the questions/day and answers/question graphs - line graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    # data3DP_nodates_QA=[data[0],data[5]]
    # headings_QA=[headings[0],headings[5]]
    data3DP_nodates_QA = data[0:6:5]
    headings_QA = headings[0:6:5]

    plotAllGraphsAx(dates, data3DP_nodates_QA, headings_QA, 'Questions/day and Answers/question')


# ######################### Plotting by index - start ###############################
# #                                                                                 #
# #                                Plotting by index                                #
# #   (eliminates need for multiple functions all doing basically the same thing)   #
# #                                                                                 #
# ######################### Plotting by index - start ###############################

def plotIndexGraphAxHead(dates, data, headings, index=5):
    """Individual graph - Plots the individual index graph - line graph - one set of axes"""
    """Defaults to answers per question"""
    logit(f'Entering {thisFunctionName()}()')
    logit(f'headings[{index}]:\n{headings[index]}')
    # Need brackets around a single heading label, https://stackoverflow.com/a/66372700/4424636
    # plotAllGraphsAxHead(dates, [data[index]], [headings[index]],
    #                     headings[index])  # https://stackoverflow.com/a/66372700/4424636
    plotAllGraphsAxHead(dates, [data[index]], [''],
                        headings[index])  # https://stackoverflow.com/a/66372700/4424636


def plotIndexGraphAx(dates, data, headings, ax, index=5):
    """Individual graph - Plots the individual index graph - line graph - one set of axes"""
    """Defaults to answers per question"""
    logit(f'Entering {thisFunctionName()}()')
    logit(f'headings[{index}]:\n{headings[index]}')
    # Need brackets around a single heading label, https://stackoverflow.com/a/66372700/4424636
    # plotAllGraphsAx(dates, [data[index]], [headings[index]], ax,
    #                 title=headings[index])  # https://stackoverflow.com/a/66372700/4424636
    plotAllGraphsAx(dates, [data[index]], [''], ax,
                    title=headings[index])  # https://stackoverflow.com/a/66372700/4424636


def selectIndexGraph(dates, data, headings, flags):
    """Run through the flags list and plot the appropriate graphs (if any)"""
    logit(f'Entering {thisFunctionName()}()')

    indices = [i for i, x in enumerate(flags) if x]
    print(f'indices:\n{indices}')
    for index in indices:
        # plotIndexGraph(dates, data, headings, index)
        plotIndexGraphAxHead(dates, data, headings, index)


# ############################ Stacked graphs - start ###############################
# #                                                                                 #
# #                                 Stacked graphs                                  #
# #                                                                                 #
# ############################ Stacked graphs - start ###############################

def plotAllUsersGraphsStackAxHead(dates, data, headings):
    """Combined graph - Plots 200, 2k and 3k user rep graphs - stack graph - one set of axes TODO Not correct"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    headings_allUsers = headings[2:5]
    plotAllGraphsStackAxHead(dates, data3DP_nodates_allUsers, headings_allUsers, title='User reputations', yLabel='Number of Users')


def plotAllUsersGraphsStackAx(dates, data, headings, ax):
    """Combined graph - Plots 200, 2k and 3k user rep graphs - stack graph - one set of axes TODO Not correct"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    headings_allUsers = headings[2:5]
    plotAllGraphsStackAx(dates, data3DP_nodates_allUsers, headings_allUsers, ax, title='User reputations', yLabel='Number of Users')


def plotAllUsersPureGraphsStackAxHead(dates, data, headings):
    """Combined graph - Plots pure 200, 2k and 3k user rep graphs - stack graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    data3DP_nodates_allUsersPure = getPureUsers(data3DP_nodates_allUsers)
    headings_allUsers = headings[2:5]
    plotAllGraphsStackAxHead(dates, data3DP_nodates_allUsersPure, headings_allUsers, title='User reputations', yLabel='Number of Users')


def plotAllUsersPureGraphsStackAx(dates, data, headings, ax):
    """Combined graph - Plots pure 200, 2k and 3k user rep graphs - stack graph - one set of axes"""
    logit(f'Entering {thisFunctionName()}()')

    data3DP_nodates_allUsers = data[2:5]
    data3DP_nodates_allUsersPure = getPureUsers(data3DP_nodates_allUsers)
    headings_allUsers = headings[2:5]
    plotAllGraphsStackAx(dates, data3DP_nodates_allUsersPure, headings_allUsers, ax, title='User reputations', yLabel='Number of Users')


def plotQAGraphsStackAxHead(dates, data, headings):
    """Combined graph - Plots the questions/day and answers/question graphs - stack graph - one set of axes TODO Not correct"""
    logit(f'Entering {thisFunctionName()}()')

    # data3DP_nodates_QA=[data[0],data[5]]
    # headings_QA=[headings[0],headings[5]]
    data3DP_nodates_QA = data[0:6:5]
    headings_QA = headings[0:6:5]
    plotAllGraphsStackAxHead(dates, data3DP_nodates_QA, headings_QA)


# ######################### Plotting by index - start ###############################
# #                                                                                 #
# #                                Plotting by index                                #
# #   (eliminates need for multiple functions all doing basically the same thing)   #
# #                                                                                 #
# ######################### Plotting by index - start ###############################

def plotIndexGraphStackAxHead(dates, data, headings, index=5):
    """Individual graph - Plots the individual index graph - stack graph - one set of axes"""
    """Defaults to answers per question"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'headings[{index}]:\n{headings[index]}')
    # Need brackets around a single heading label, https://stackoverflow.com/a/66372700/4424636
    # plotAllGraphsStackAxHead(dates, [data[index]], [headings[index]],
    #                          headings[index])  # https://stackoverflow.com/a/66372700/4424636
    plotAllGraphsStackAxHead(dates, [data[index]], [''],
                             headings[index])  # https://stackoverflow.com/a/66372700/4424636


def plotIndexGraphStackAx(dates, data, headings, ax, index=5):
    """Individual graph - Plots the individual index graph - stack graph - takes ax as arg"""
    """Defaults to answers per question"""
    logit(f'Entering {thisFunctionName()}()')

    logit(f'headings[{index}]:\n{headings[index]}')
    # Need brackets around a single heading label, https://stackoverflow.com/a/66372700/4424636
    # plotAllGraphsStackAx(dates, [data[index]], [headings[index]], ax,
    #                      title=headings[index])  # https://stackoverflow.com/a/66372700/4424636
    plotAllGraphsStackAx(dates, [data[index]], [''], ax,
                         title=headings[index])  # https://stackoverflow.com/a/66372700/4424636


def selectIndexGraphStack(dates, data, headings, flags):
    """Run through the flags list and plot the appropriate graphs (if any)"""
    logit(f'Entering {thisFunctionName()}()')

    indices = [i for i, x in enumerate(flags) if x]
    print(f'indices:\n{indices}')
    for index in indices:
        # plotIndexGraphStack(dates, data, headings, index)
        plotIndexGraphStackAxHead(dates, data, headings, index)


# ### Second Level ###

def RetrieveDataStore():
    """Check for DataStore. Return CSV data"""
    logit(f'Entering {thisFunctionName()}()')

    from Area51Scraper import readData
    from Area51Scraper import checkDirs
    if checkDirs():
        data = readData()
    else:
        data = None

    return data


def RetrieveWebData(url):
    """Take URL. Return CSV data"""
    logit(f'Entering {thisFunctionName()}()')

    data = getData2(url)
    logit(f'Retrieved data:\n{data}')

    data = rejoinKUsers(data)
    logit(f'Data after user headings rejoined:\n{data}')

    data = stringStripper(data)
    logit(f'Data after strings stripped:\n{data}')

    return data


def reorganiseData(data):
    """Take CSV data. Return dates, pure data, and headings"""
    logit(f'Entering {thisFunctionName()}()')

    sanityCheckDates(data)
    data3DP_dates = extractDates(data)
    logit(f'Dates:\n{data3DP_dates}')

    convertDate(data3DP_dates[0])
    logit(f'Date:\n{data3DP_dates[0]} is {convertDate(data3DP_dates[0])}')

    data3DP_pydates = convertDates(data3DP_dates)
    logit(f'PyDates:\n{data3DP_pydates}')

    data3DP_noDates = extractData(data)
    logit(f'Data:\n{data3DP_noDates}')

    data3DP_headings = extractHeadings(data)
    logit(f'Headings:\n{data3DP_headings}')
    data3DP_headings = stripAsterisksFromHeadings(data3DP_headings)
    logit(f'Headings:\n{data3DP_headings}')

    return data3DP_pydates, data3DP_noDates, data3DP_headings


def plotData(dates, data, headings):
    """Take dates, pure data, and headings. Plot graphs"""
    """ 
    Graphing Permutations:

    Questions per day
    Answer Rate
    AllUsers
    Users200
    Users2k
    Users3k
    Answers per question
    Questions & Answers
    Visits per day
    All on one sheet
    Stacked
    """
    logit(f'Entering {thisFunctionName()}()')

    # All Line/Stack graphs  ########################################################

    # Plot graphs one at a time
    if flagAllGraphs:
        plotAllGraphsIndividually(dates, data, headings)

    # Plot all graphs on same axes
    if flagAllGraphs and flagOneSheet:
        plotAllGraphsAxHead(dates, data, headings)

    # Combination Line/Stack graphs  ########################################################

    # Plot number of users graphs (3 graphs)
    if flagAllUsers:
        if flagStacked:
            plotAllUsersPureGraphsStackAxHead(dates, data, headings)
        else:
            if flagPure:
                plotAllUsersPureGraphsAxHead(dates, data, headings)
            else:
                plotAllUsersGraphsAxHead(dates, data, headings)

    # Plot questions/day and answers per question graphs (2 graphs)
    if flagQuestionsAndAnswers:
        if flagStacked:
            plotQAGraphsStackAxHead(dates, data, headings)
        else:
            plotQAGraphsAxHead(dates, data, headings)

    # Individual Line/Stack graphs  ########################################################

    if flagStacked:
        selectIndexGraphStack(dates, data, headings, flagsIndividual)
    else:
        selectIndexGraph(dates, data, headings, flagsIndividual)

    # Plot with parasite Y-axis
    if flagParasite:
        if flagParasiteAll:
            plot4YAxisGraph(dates, data, headings)
        else:
            # plotParasite3Graph(dates, data, headings)
            plot3YAxisGraph(dates, data, headings)

    # Plot two graphs in one figure
    if flagDualAxes:
        plotTwoGraphs(dates, data, headings)

    # We can not plot all graphs together as the Y-axes differ.

    # Similar Y-axis are:
    #  Number of users (200, 2k, 3k)
    #  Questions/day and Answers per Question - have different Y-axes on left- and right-hand side of graph.
    #    It would be good to show % answered also shown on this graph but there are only two axes - can we put a third y axis?
    #    Can we also fit on visits per day? - That would mean only two graphs are required  - can we put a fourth y axis?


def retrieveData(data):
    """Get the data from DataStore or Web (if need be) or default to built-in"""
    logit(f'Entering {thisFunctionName()}()')

    flagHaveData = False
    if flagUseDataStore:
        print('--- Using DataStore')
        dataStore = RetrieveDataStore()

        # If local DataStore does exist
        if dataStore is not None:
            print('--- DataStore found')
            data = dataStore
            flagHaveData = True
        else:
            print('--- DataStore not found')

    # Import errors for except
    # From https://stackoverflow.com/a/65991585/4424636
    import urllib.error
    import socket
    if flagWeb and not flagHaveData:
        print('--- Using web data')
        try:
            dataWeb = RetrieveWebData(url3DP)
        except urllib.error.URLError:
            print('--- Web data retrieval error')
            dataWeb = None
        except socket.gaierror:
            print('--- Web data retrieval error')
            dataWeb = None
        if dataWeb is not None:
            print('--- Web data found')
            flagHaveData = True
            data = dataWeb
        else:
            print('--- Web data not found')
    # else:
    #     print('--- Using local data')
    if not flagHaveData:
        print('--- Using local data')

    return data


# ### Top Level ###
def main():
    """This is where it all begins"""
    logit(f'Entering {thisFunctionName()}()')

    global data3DP
    setFlags()

    logit(f'flagUseRC before:\n{flagUseRC}')
    logit(f'flagUseRC.type before:\n{type(flagUseRC)}')

    if flagUseConfigFile:
        globalsConfig3()
    logit(f'flagUseRC after:\n{flagUseRC}')
    logit(f'flagUseRC.type after:\n{type(flagUseRC)}')

    if flagUseRC:
        rcSettings()
    else:
        rcDefaults()

    data3DP = retrieveData(data3DP)

    dates, data, headings = reorganiseData(data3DP)

    # DataFrame Stuff
    # createDataFrameHorizontal(dates, data, headings)
    # data=interspliceData(data)
    # createDataFrameVertical(dates, data, headings)

    plotData(dates, data, headings)


if __name__ == "__main__":
    main()

# ####### THis is the TO DO Section ################################################

# TODO: no legend on single graphs
# TODO: Dual axes in one fig
# TODO: Different colours in multiple line plots
# TODO: Use generic index plot for individual plots
# TODO: put flags into an array or list for cycling through


# TODO: Remove legend on individual - it has come back
# TODO Not tight 4 y - or the figure size is too small
# TODO Size of plotallgraphs - done

# rename num to str2num
# TODO Check capitalisation of function names
# Comments on all the parameters
# Parameter for slanted dates
# TODO Split plot graphs (i.e. plotGraph()) and plotAllGraphs for gave ax and fig context (subplot?)

# TODO change name of parasite flags - both of them
# Check if configfile exists


# TODO Check size of 3 axis parasite (8, 5)
# check size of 4 axis parasite(9,5)
# TODO check size of standard 2 axis
# TODO Have different sizes of graph parameter
# need global declaration in settings2()
# Add flagUseConfig
# Change window title
# TODO "ha" in rc ticks - not working in rc settings
# TODO Config file - variable types
# TODO Config file - get type before settings and then reset the type?
# TODO fig size in rc settings
# TODO fig size - are there "classes" of plots that could have different sizes, i.e. "standard"/"2Y", "3Y" and "4Y"?
# TODO fig size - base it on the number of y axis => 2/3/4?
# TODO check if possible to use rcParams()

# TODO flagSameRange on three Y axis
# TODO flagSameRange on three Y axis - specify same limits on which axes?
# TODO flagSameRange on four Y axis - specify same limits on which axes?
# TODO flagSameRange specify which indices of Y-axis dataset used on same range
# flagSameRange specific range for the same axes - currently hard wired to (0, 5)
# TODO make generic 4 axis - similar to the 3 axis
# TODO make generic 4 axis - specify lim for AR% on which axis?
# TODO make generic 3 axis - specify lim for AR% on which axis? Maybe there is no AR%?
# TODO flagSameRange make same range use the same axis

# Put dates on three axis graph

# TODO fix cfg read in a loop
# TODO Make axis three and four generic - join together in one function?
# TODO Make 2,3,4 generic (or just the head), and base figsize on number of Y axes, pass num as parameter
# TODO ...or just stick with the biggest size needd for the four axes
# TODO Figsize is only a problem for dates and the legend

#  Use "datastore" (from Area51Scraper) as offline data source
#  Add a flag for the datastore use - flagUseDataStore
#  Check logic of web read if datastore.
#  Add exception handling for web if Wi-Fi turned off
#  Add more robust data retrieval check to web (similar to datastore check for None)
# TODO Tidy the exception handling in the web retrieval - put in a function?
# TODO Specify location of the parasite axes as a flag - cfgParasiticAxesLocation - done
# TODO Check moving axis to left/right, using the same four lines
# TODO Check moving axis - what displacement is required? 120 = 2 x 60
# TODO Fix the spines disparity between 3Y and 4Y routines

# TODO Put the labeltick rotation in the end of the head? The dates on all axes should be oriented the same, anyway
# TODO Add date ticks at start and end of x-axis (currently only in the middle)

#  Docs: explain the building of tuple and list in config reading = done (I think)
#  Docs: flat dates and no ha dates (plus the last two ha) = done

# TODO Why does two graph plot -4 axis encroaches on to the other plot?

# Fix naming of config files: graph_config.cfg -> graph_config_1.cfg; add a sym link to graph_config.cfg?;

# TODO plotAllGraphsAx() not used - related to:
# TODO Line Graph functions are not yet split, AX() are leacking ax parameter and Ax()/AXHead() are all the same:
#   TODO plotAllUsersGraphsAxHead(), )plotAllUsersGraphsAx(), )plotAllUsersPureGraphsAxHead(),
#   TODO plotAllUsersPureGraphsAx(), plotQAGraphsAxHead(), )plotQAGraphsAx()

# Add shiftable y axis (left or right) to plotParasite3GraphGenericAx()
# Prepare 4Y plot for Ax() - Dataset is only prepared for AxHead() - Plot 2 graphs doesn't call Prepare4Y()
