# SE3DP_PlotterScraper

A set of Python 3.6 scripts which scrape data from the Stack Exchange Area51 page for the 3D Printing site and plot the obtained historical statistics. These scripts consist of:

 - A Python script which plots various statistical graphs relating to the Stack Exchange 3D Printing site (referenced henceforth as *SE.3DP*), using data taken from a [SE.3DP Meta page](https://3dprinting.meta.stackexchange.com/q/264/4762).
 - A Python script which scrapes daily statistics from the [Area51 page for SE.3DP](https://area51.stackexchange.com/proposals/82438/3d-printing).

# Historical Perspective

Back in May 2018 a [question](https://3dprinting.meta.stackexchange.com/q/264/4762) was asked on the Stack Exchange 3D Printing Meta site asking about the *Beta* status of the site. [One of the answers](https://3dprinting.meta.stackexchange.com/a/265/4762) provided data, taken from the [Area51 page for the 3D Printing site](https://area51.stackexchange.com/proposals/82438/3d-printing), showing the statistics for the site, and their relevance to the site losing its Beta status. These statistics are:

 - *Questions per day*
 - *Answer rate*
 - *Users*
   - *200+ reputation*
   - *2,000+ reputation* 
   - *3,000+ reputation*
 - *Answers per question* ratio
 - *Visits per day*

These statistics were then updated and maintained over time and additional formats of that same present were provided, in particular a CSV format.

Three years later, it seemed like a good idea to plot these historical statistics over time, just to see how the site has progressed. It is this CSV data that the graph plotting script uses to plot the graphs.

In order to simplify the maintenance of the statistics and CSV stored on that Meta page, an Area51 web page scraping script was also developed.

# Overview

## Graph Plotter

![Dual Graph](https://gr33nonline2.files.wordpress.com/2020/12/se3dp-graph-dual-left-left.png)

The graph plotter is a highly configurable script that plots graphs, using the `matplotlib` library (`v3.4`), of the statistics of the SE.3DP site. These graphs can take the form of:

 - Single line plots
 - Multiple (3 or 4) line plots
 - Stacked graphs - for user reputation
 - Dual axes figures - two graphs shown side-by-side (as demonstrated in the image above)

The data for the graphs is sourced from one of three locations, in the following priority:

 1. DataStore - this is a Pickle of the SE.3DP stats, created by the Area51 scraper script.
 2. SE.3DP Meta page - to be precise, the CSV data in this [answer](https://3dprinting.meta.stackexchange.com/a/265/4762) to the Meta question [What does it take to get out of Beta stage?](https://3dprinting.meta.stackexchange.com/q/264/4762)
 3. Built-in - there is a (probably out of date) dataset hardcoded into the script.

If any of the sources are not available (i.e. DataStore not yet created, or Meta page unreachable due to lack of internet access), then the script falls back to the next source, defaulting finally to the built-in data.

## Area51 Scraper

When run, the scraper will scrape the [Area51 page for the 3D Printing site](https://area51.stackexchange.com/proposals/82438/3d-printing) and output two date/time stamped files:

 - a DataStore `pickle` file (`.pkl`) containing the data structures internal to the script. This DataStore is used by both the Scraper and the Plotter. This file should *not* be opened by the user.
 - a `text` file (`.txt`) containing the raw CSV, and the HTML lists. This file is intended to be opened by the user, and the contents copied and pasted in to the [SE.3DP Meta page](https://3dprinting.meta.stackexchange.com/q/264/4762) to update [the answer](https://3dprinting.meta.stackexchange.com/a/265/4762).

These two files are stored within a directory named `DataStore/` which will be automatically created if it does not already exist. For further information see the section **Files** - **Other Files**, below.

The script will also output the raw CSV, and the two HTML formatted lists to the command line (i.e. console). 

The Area51 scraper can be run as often as required. The first time it is run in any particular 24-hour period (starting 00:00 GMT), data is scraped from the Area51 page for 3D Printing, and added to the DataStore (as both a binary `pickle` file, and a user-friendly text file). When, or if, the script is run thereafter, in the *same* 24-hour period, the script will detect this and will not scrape nor add additional data to the DataStore. It will, however, display the results again on the console.

# Super Quick Start

To get started simply:

 1. Download the repository ZIP file.
 2. Unzip the ZIP file
 3. `cd` into the resulting directory
 4. Run `Area51Scraper.py`
 5. Run `StackExchange3DP_6.py`

# Quick Start

The two scripts take no command line arguments, and can be run "as is" without requiring any additional setup:

 - The graph plotter, `StackExchange3DP.py`, has an extensive number of options available and can be configured by editing the configuration file. However, the default configuration is sufficient to produce the required graphs, without requiring the need to make any edits prior to running the script for the first time. 
 - The Area51 scraper, `Area51Scraper.py` is quite straightforward and not configurable. It can be executed without any additional setup. 
 
**Note**: The scripts are independent of each other and do not *need* to be run in any particular order. However, running the Area51 scraper before running the Graph Plotter *will* provide a more up-to-date set of graphs.

## Files

### The Latest File Versions

There are a number of versions of the same scripts/files, so these are the latest versions that should be used:

 - `StackExchange3DP_6.py` - The graph plotter
 - `Area51Scraper.py` - The Area51 scraper
 - `Resources/graph_config_3.cfg` - The configuration file for the graph plotter

The other files - versions of the same - can be safely disregarded as they are present purely as a record of the developmental history.

### Other Files

Also worthy of note, is the **DataStore**, which contains the following files:

 - `DataStore/3DPCSV_<timestamp>.pkl`
 - `DataStore/3DPCSV_<timestamp>.txt`
 - `DataStore/3DPCSV.pkl`
 - `DataStore/3DPCSV.txt`

The `.pkl` (`pickle`) file is **not** for users to access, and is used for internal storage of the datasets that are used by both the graph plotter and the web scraper.

The `.txt` (`text`) file is for users to access, and contains a copy of the CSV, and the HTML formatted lists that can be copied and pasted into the SE.3DP Meta page.

Both `DataStore/3DPCSV.pkl` and`DataStore/3DPCSV.txt` are copies of the *last* (i.e. most recent) respective timestamped files, but without the timestamp in the filename. These are generated only for convenience of the user.

## Configuration Details

### The Plotter

The graph plotter is highly configurable. The configuration file is well commented and *should* be self-explanatory... with a little experimentation. The configuration is fairly bomb-proof, and most configuration errors *should* be caught. Where appropriate, defaults are used in the case of mis-configuration.

Do not change the configuration settings in the script itself, but rather use the separate user configuration file - that is what it is for. That said, if the configuration file is missing, then the script will run using the settings built into the script itself.

A sample of what can be configured:

 - Which graphs are plotted:
   - Individual
   - Combined
   - Line plots
   - Stacked plots
   - Multi-line plots (3 or 4 lines)
   - Dual graph plots
 - Save graphs to file:
   - PDF
   - PNG
   - DPI
 - Rotation of the dates
 - Graph size
 - Location of the parasitic Y axes on the three and four plot graphs - left or right or combined
 - Debugging output on or off
 - Plot colours
 - Plot markers
 - This list is endless...

### The Scraper

There is nothing to configure... simples!

## Environment

Installing PyCharm to run the scripts, is recommended, although it is not necessary as the scripts can be easily run from the command line. However, using PyCharm saves typing and makes running the scripts a simple one-click operation, as well as providing a GUI within which to operate. It also eases the installation of the libraries required by the scripts.

### Prerequisites

Python 3.6

#### libraries

 - matplotlib 3.4
 - BeautifulSoup
 - configparser
 - re
 - pandas

# Developer Notes

Development notes can be found at gr33nonline WordPress blog: [Plotting SE3DP stats](https://gr33nonline.wordpress.com/2021/04/13/plotting-se3dp-stats/)

# Further Work

In both scripts, there are a lot of `# TODO` comments scattered throughout the code, in particular at the bottom of the scripts, as well as at the end of the [development notes](https://gr33nonline.wordpress.com/2021/04/13/plotting-se3dp-stats/). 

There is also a number of uncalled functions, which are in place for future work, and not yet complete. These will be addressed in future versions. Superceded functions have (for the most part) been commented out, and these will be removed in future versions.

These scripts, while (*now*) fully functional, *are* works in progress and will be ameliorated and updated, from time to time.

# Future Expansion

Work is underway on an SQL query that will obtain a dataset starting from when the 3D Printing site was first conceived, rather than from when data was first recorded on the SE.3DP Meta page. This will probably make the CSV data on SE.3DP Meta page redundant, at some point.

An additional GUI, to simplify the configurtion the config file.

DataFrame use.

# Untested

**If you update the Meta page answer using the Area51 scraper output, you should probably (manually) delete the DataStore, and allow it to be recreated by re-running the scraper**. This is because it is not clear what will happen if:
 - (a) the DataStore contains the *same* data as the Meta page (with all of the same dates present) - it should be OK . For example, if the Meta page is updated (using data from the DataStore), *after* the initial creation of the DataStore
 - (b) the DataStore contains the *different* data as the Meta page (with some of the dates differing) - undefined output. For example, if the Meta page is updated using data from a *different* (i.e. someone else's) DataStore, *after* the initial creation of the local DataStore.

# History

## Graph Plotter

```
# StackExchange3DP_6.py
# Removed redundant code, leaving:
#   Individual by Index graph only
#   Stack graphs for Users only
#   Case-Switch for getData()
# Moved graph rc stuff to function and added rcdefaults()
# Adding config file
# Moved global flags settings (individual and combined graphs) to function
# Split StringStripper() to have StringStripperRow() as well - StringStripperRow() can specify start
# TODO - remove old function names from print dictionary
# Added cfgSameRange
# Added window title
# Added rotated dates
# Added cfg for location of parasitic axes
# Added flag for DataStore from Area51 scraper - overrides Web retrieve.
# If DataStore isn't present then the built-in data is used
# Added exception handling for when no network is available during web retrieval.
# Renamed num() to str2num()
# Made parasitic Y axes relocatable (for both 3 and 4 Y axes)
# Split the parameter preparation for the 3 and 4 Y axis parasitic graphs into separate function
# Renamed flagParasiticThree to cfgThreePlotDataSet, added cfgFourPlotDataSet


# StackExchange3DP_5.py
# Added beautiful soup scrape - This was added in v4
# Moved imports to top
# Added graph save to all plots
# Added \n to all logit() calls
# Added dual plot
# Split plot functions into Ax and AxHead:
#   plotAllGraphsStack
#   plotParasite4Graph
#
#   plotIndexGraph
#   plotIndexGraphStacked
#   plotParasite3GraphGeneric
# Added plot save to:
#   plotAllGraphsStack
#   plotGraph
#   plotAllGraphs


# StackExchange3DP_4.py
# Moved main into three functions
# Code tidied up
# Introduced flags list for individual index graph
# Introduced User rep data processing for stacked graphs
# Added DataFrames (vertical and horizontal) (but not implemented)
# Colour Cycling for line graphs
# Stack graph data fix for User rep
# No legend for individual graphs
# Introduced case-switch for get data
# Half implemented BeautifulSoup web scrap - works but messy
# Added parasite graphs (three and four way)
# Dictionary switcher for getData method
# Half added PDF graph output
# Half added PNG graph output
# Added time stamp for files saved
# Fixed Regex web scrape

# StackExchange3DP_3.py
# logit(), all graphs and flags added

# StackExchange3DP_2.py
# Web retrieval - String Search and Regex. Regex not working
# More graphs added

# StackExchange3DP_1.py
# Data reorganisation
# Simple graph plot
```

## Web Scraper

No history as yet... initial version.
